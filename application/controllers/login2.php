<?php
class Login2 extends CI_Controller {

	
	public function index()
	{
        //$data['main_content'] = 'login_view2';
        //$this->load->view('includes/template', $data);

        $data['content'] = '<form method="post" role="form" accept-charset="utf-8" action="http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/login2/validate_credentials" />';
        $data['content'] .='    <div class="form-group">';
        $data['content'] .='        <input class="form-control" placeholder="username" id="username" name="username" autofocus>';
        $data['content'] .='    </div>';
        $data['content'] .='    <div class="form-group">';
        $data['content'] .='        <input type="password" placeholder="password" class="form-control" id="password" name="password">';
        $data['content'] .='    </div>';
        $data['content'] .='    <button type="submit" value="Login" class="btn btn-lg btn-success btn-block">Login</button>';
        $data['content'] .= '<br /><button onclick="window.location=\'signup\';" type="button" class="btn btn-lg btn-primary btn-block">Student Sign Up</button>';
        $data['content'] .= '<button onclick="window.location=\'judge_signup\';" type="button" class="btn btn-lg btn-warning btn-block">Judge Sign Up</button>';
        $data['content'] .= '<br /><p class="text-right"><a href="http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/forgot_password">Forgot Password</a></p>';



        $this->load->view('login_view2', $data);
	}
	
    function validate_credentials()
    {
        $this->load->model('membership_model');
        $query = $this->membership_model->validate();
        
        
        if($query[0] == 1)
        {
            if($query[1] == 1)
            {
                $data = array(
                    'username' => $this->input->post('username'),
                    'is_logged_in' => true,
                    'acc_level' => 1
                );
            
                $this->session->set_userdata($data);
                redirect('admin');
            }

            elseif($query[1] == 2)
            {
                $data = array(
                    'username' => $this->input->post('username'),
                    'is_logged_in' => true,
                    'acc_level' => 2
                );
            
                $this->session->set_userdata($data);
                redirect('judge');
            }

            elseif($query[1] == 3)
            {
                $data = array(
                    'username' => $this->input->post('username'),
                    'is_logged_in' => true,
                    'acc_level' => 3
                );
            
                $this->session->set_userdata($data);
                redirect('student');
            }
            
        }
        
        else
        {
            redirect('login2');
        }
    }
    
    function logout()
	{
		$this->session->sess_destroy();
		redirect('login2');
	}
    
	
	
	
	
	





}//end class Login2
