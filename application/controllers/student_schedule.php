<?php
class Student_schedule extends CI_Controller {
	//---------------------------------------------------------------------------
	//---------------------------------------------------------------------------
	//---------------------------------------------------------------------------
	//		9/1/2014 - Everything in this file has been added to student.php
	//---------------------------------------------------------------------------
	//---------------------------------------------------------------------------
	//---------------------------------------------------------------------------
	public function __construct()
	{
		parent::__construct();
		$this->load->model('debate_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->is_logged_in();
		$this->load->library('studentMenu');
	}
	
	public function index()
	{   
		$this->studentmenu->show_menu();

		//$data['dates'] = $this->debate_model->get_all_deb_days();
		$data['dates'] = $this->debate_model->get_date_options();

		// echo '<pre>';
		// echo var_dump($data['dates']);
		// echo '</pre>';

		$data['content'] = "<div id='page-wrapper'>";
		$data['content'] .= "<div class='row'>";
		$data['content'] .= "<div class='col-lg-12'>";


		$data['content'] .= '<div id="page=wrapper">';
		$data['content'] .= '<div class="row">'; 
		$data['content'] .= "                <div class='col-lg-12'>"; 
		$data['content'] .= "                    <div class='panel panel-default'>"; 
		$data['content'] .= "                        <div class='panel-heading'>"; 
		$data['content'] .= "                            Please complete the form below"; 
		$data['content'] .= "                        </div>"; 
		$data['content'] .= "                        <div class='panel-body'>"; 
		$data['content'] .= "                            <div class='row'>"; 
		$data['content'] .= "                                <div class='col-lg-6'>";

		$data['content'] .= "<form role='form' action='./student_schedule/insert_schedule' method='post'>";
		$data['content'] .= "<div class='form-group'>";
		//$data['content'] .= "<label>Participant Type</label>";

		$count = 1;
		// foreach($data['dates'] as $d)
		// {
		// 	$dateTimeID = $d->dateTimeID;
		// 	$time = $d->start;
		// 	$day = $d->date;
		// 	//echo $day."<br />";
		// 	//$data['content'] .= "$day<br />";

		// 	$collapse_href = '#collapse'.$count;
		// 	////CREATE A FUNTION in the model that just returns an array of dateIDs from the 'Date' table
		// 	////CREATE A SEPARATE array that has 

		// 	//unique date IDs
		// 	//	1-25 times
		// 	//next unique date ID
		// 	//	1-25 times


		// 	$data['content'] .= '<div class="panel-group" id="accordion">';

	 //  		$data['content'] .= '<div class="panel panel-default">';
	 //  		$data['content'] .= '    <div class="panel-heading">';
	 //  		$data['content'] .= '      <h4 class="panel-title">';
	 //  		$data['content'] .= '        <a data-toggle="collapse" data-parent="#accordion" href="$collapse_href">';
	 //  		$data['content'] .=          $day;
	 //  		$data['content'] .= '        </a>';
	 //  		$data['content'] .= '      </h4>';
	 //  		$data['content'] .= '    </div>';
	 //  		$data['content'] .= '    <div id="collapseOne" class="panel-collapse collapse in">';
	 //  		$data['content'] .= '      <div class="panel-body">';

	 //  		//CHECKBOXES
	 //  		$num = 1;
	 //  		while($num < 26)
	 //  		{
	 //  			$data['content'] .= "<div class='checkbox'>";
		// 		$data['content'] .= "<label>";
		// 		$data['content'] .= "<input type ='checkbox' name='date_range[options][]' value='$dateTimeID'>$time";
		// 		$data['content'] .= "</label>";
		// 		$data['content'] .= "</div>";

		// 		$num++;
	 //  		}
			

		// 	$data['content'] .= '      </div>'; //panel-body
	 //  		$data['content'] .= '    </div>'; //collapseOne
	 //    	$data['content'] .= '</div>'; //panel panel-default

	 //    	$data['content'] .= '</div>'; //panel-group
  // 		}



		foreach($data['dates'] as $c)
		{   
			$dateTimeID = $c->dateTimeID;
			$day = $c->date;
			$start_time = $c->start;
			$end_time = $c->end;

			$option = $day.' at '.$start_time.'<br />';

			$data['content'] .= "<div class='checkbox'>";
			$data['content'] .= "<label>";
			$data['content'] .= "<input type='checkbox' name='date_range[options][]' value='$dateTimeID'>$option";
			$data['content'] .= "</label>";
			$data['content'] .= "</div>";

			$count++;

			//echo $begin.' -- '.$end.'<br />';
		}


		$data['content'] .= "</div><!-- form-group -->";
		$data['content'] .= "<button type='submit' class='btn btn-default'>Submit</button>";
		$data['content'] .= "<button type='reset' class='btn btn-default'>Reset</button>";
		$data['content'] .= "</form>";
		$data['content'] .= "</div>"; 
		$data['content'] .= "                                <!-- /.col-lg-6 (nested) -->"; 
		$data['content'] .= "                            </div>"; 
		$data['content'] .= "                            <!-- /.row (nested) -->"; 
		$data['content'] .= "                        </div>"; 
		$data['content'] .= "                        <!-- /.panel-body -->"; 
		$data['content'] .= "                    </div>"; 
		$data['content'] .= "                    <!-- /.panel -->"; 
		$data['content'] .= "                </div>"; 
		$data['content'] .= "                <!-- /.col-lg-12 -->"; 
		$data['content'] .= "            </div>";


		$data['content'] .= "</div>";
		$data['content'] .= "</div>";
		$data['content'] .= "</div>";


        $this->load->view('student/student_view3', $data);
        //$this->load->view('student/student_schedule_view', $data);
	}
    
    function insert_schedule()
    {

    	$this->debate_model->insert_student_schedule();
    	//redirect('/student_schedule/submit_success','refresh');
    }
	
	function submit_success()
	{
		$data['content'] = "<div class='alert alert-success alert-dismissable'>";
		$data['content'] .= "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
		$data['content'] .= "Thank you! Your times have been submitted.";
		$data['content'] .= "</div>";

		$this->load->view('student/student_schedule_view', $data);
	}

	function is_logged_in()
    {
        $is_logged_in = $this->session->userdata('is_logged_in');
        
        if(!isset($is_logged_in) || $is_logged_in != true)
        {
            echo 'You don\'t have permission to access this page. <a href="../login2">Login</a>';
            die();
        }
    }
    
	
	
	
	
	





}
