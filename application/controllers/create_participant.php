<?php
class Create_participant extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('debate_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->is_logged_in();
	}

	function is_logged_in()
    {
        $is_logged_in = $this->session->userdata('is_logged_in');
        
        if(!isset($is_logged_in) || $is_logged_in != true)
        {
            echo 'You don\'t have permission to access this page. <a href="./login2">Login</a>';
            die();
        }
    }
	
	public function index()
	{   
        $this->load->view('admin/participant_form');
	}
    
    public function insert_participant()
    {
        $this->debate_model->insert_participant();
        //$this->debate_model->send_mail();
        redirect('/create_participant/','refresh');
    }
	
    
	
	
	
	
	





}
