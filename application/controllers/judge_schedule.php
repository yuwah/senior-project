<?php
class Judge_schedule extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('debate_model');
		$this->load->model('judge_debate_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->is_logged_in();
	}
	
	public function index()
	{   
		$data['dates'] = $this->debate_model->get_date_options();
		$data['content'] = '<div class="row">'; 
		$data['content'] .= "                <div class='col-lg-12'>"; 
		$data['content'] .= "                    <div class='panel panel-default'>"; 
		$data['content'] .= "                        <div class='panel-heading'>"; 
		$data['content'] .= "                            Please complete the form below"; 
		$data['content'] .= "                        </div>"; 
		$data['content'] .= "                        <div class='panel-body'>"; 
		$data['content'] .= "                            <div class='row'>"; 
		$data['content'] .= "                                <div class='col-lg-6'>";

		$data['content'] .= "<form role='form' action='./judge_schedule/insert_schedule' method='post'>";
		$data['content'] .= "<div class='form-group'>";
		//$data['content'] .= "<label>Participant Type</label>";

		$count = 1;
		foreach($data['dates'] as $c)
		{   

			$day = $c->date;
			$start_time = $c->start;
			$end_time = $c->end;


			$begin_unix = $day + $start_time;
			$end_unix = $day + $end_time;

			$begin = date("D F j, Y, g:i a", $begin_unix);
			$end = date("D F j, Y, g:i a", $end_unix);

			$option = $begin.' -- '.$end.'<br />';

			$data['content'] .= "<div class='checkbox'>";
			$data['content'] .= "<label>";
			$data['content'] .= "<input type='checkbox' name='date_range[options][]' value='$count'>$option";
			$data['content'] .= "</label>";
			$data['content'] .= "</div>";

			$count++;

			//echo $begin.' -- '.$end.'<br />';
		}


		$data['content'] .= "</div><!-- form-group -->";
		$data['content'] .= "<button type='submit' class='btn btn-default'>Submit</button>";
		$data['content'] .= "<button type='reset' class='btn btn-default'>Reset</button>";
		$data['content'] .= "</form>";
		$data['content'] .= "</div>"; 
		$data['content'] .= "                                <!-- /.col-lg-6 (nested) -->"; 
		$data['content'] .= "                            </div>"; 
		$data['content'] .= "                            <!-- /.row (nested) -->"; 
		$data['content'] .= "                        </div>"; 
		$data['content'] .= "                        <!-- /.panel-body -->"; 
		$data['content'] .= "                    </div>"; 
		$data['content'] .= "                    <!-- /.panel -->"; 
		$data['content'] .= "                </div>"; 
		$data['content'] .= "                <!-- /.col-lg-12 -->"; 
		$data['content'] .= "            </div>";


        $this->load->view('judge/judge_schedule_view', $data);
	}
    
    function insert_schedule()
    {
    	$this->judge_debate_model->insert_judge_schedule();
    	redirect('/judge_schedule/submit_success','refresh');
    }
	
	function submit_success()
	{
		$data['content'] = "<div class='alert alert-success alert-dismissable'>";
		$data['content'] .= "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
		$data['content'] .= "Thank you! Your times have been submitted.";
		$data['content'] .= "</div>";

		$this->load->view('judge/judge_schedule_view', $data);
	}

	function is_logged_in()
    {
        $is_logged_in = $this->session->userdata('is_logged_in');
        
        if(!isset($is_logged_in) || $is_logged_in != true)
        {
            echo 'You don\'t have permission to access this page. <a href="../login2">Login</a>';
            die();
        }
    }
    
	
	
	
	
	





}
