<?php
class Student extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('debate_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->is_logged_in();
		$this->load->library('studentMenu');
	}
	
	//function for student home page
	public function index()
	{
		$this->studentmenu->show_menu();

		$data['content'] =	'	<div id="page-wrapper">';
		$data['content'] .= '            <div class="row">';
		$data['content'] .= '                <div class="col-lg-12">';
		$data['content'] .= '                    <h1 class="page-header">Welcome!</h1>';
		$data['content'] .= '                </div>';
		$data['content'] .= '                <!-- /.col-lg-12 -->';
		$data['content'] .= '            </div>';
		$data['content'] .= '            <!-- /.row -->';
		$data['content'] .= '        </div>';
		$data['content'] .= '        <!-- /#page-wrapper -->';

		$data['content'] .= '    </div>';

		//$data['content'] = 'hello student';
        $this->load->view('student/student_view3', $data);
        //$this->load->view('student/student_view', $data);
	}

	//Pulls date options from db and displays them as checkbox options to the user
	//This function calls a model function that will drop the submitted times that are already in
	//	the SAvailability table and replaces them with whatever the user submits
	public function student_schedule()
	{   
		$this->studentmenu->show_menu();

		//$data['dates'] = $this->debate_model->get_all_deb_days();
		$data['dates'] = $this->debate_model->get_date_options();

		// echo '<pre>';
		// echo var_dump($data['dates']);
		// echo '</pre>';

		$data['content'] = "<div id='page-wrapper'>";
		$data['content'] .= "<div class='row'>";
		$data['content'] .= "<div class='col-lg-12'>";


		$data['content'] .= '<div id="page=wrapper">';
		$data['content'] .= '<div class="row">'; 
		$data['content'] .= "                <div class='col-lg-12'>"; 
		$data['content'] .= "                    <div class='panel panel-default'>"; 
		$data['content'] .= "                        <div class='panel-heading'>"; 
		$data['content'] .= "                            Please select the times you are available. <b>This form will completely replace any previously submitted schedules.</b>"; 
		$data['content'] .= "                        </div>";
		$data['content'] .= "                        <div class='panel-body'>"; 
		$data['content'] .= "                            <div class='row'>"; 
		$data['content'] .= "                                <div class='col-lg-6'>";

		$data['content'] .= "<form id='times' role='form' action='./insert_schedule' method='post'>";
		$data['content'] .= "<div class='form-group'>";
		//$data['content'] .= "<label>Participant Type</label>";

		$count = 1;

		foreach($data['dates'] as $c)
		{   
			$dateTimeID = $c->dateTimeID;
			$day = $c->date;
			$start_time = $c->start;
			$end_time = $c->end;

			$option = $day.' at '.$start_time.'<br />';

			$data['content'] .= "<div class='checkbox'>";
			$data['content'] .= "<label>";
			$data['content'] .= "<input type='checkbox' name='date_range[options][]' value='$dateTimeID'>$option";
			$data['content'] .= "</label>";
			$data['content'] .= "</div>";

			$count++;

			//echo $begin.' -- '.$end.'<br />';
		}


		$data['content'] .= "</div><!-- form-group -->";
		$data['content'] .= "<button type='submit' class='btn btn-success'>Submit</button>";
		$data['content'] .= "  <button type='reset' class='btn btn-primary'>Uncheck All</button>";
		$data['content'] .= "</form>";
		$data['content'] .= "</div>"; 
		$data['content'] .= "                                <!-- /.col-lg-6 (nested) -->"; 
		$data['content'] .= "                            </div>"; 
		$data['content'] .= "                            <!-- /.row (nested) -->"; 
		$data['content'] .= "                        </div>"; 
		$data['content'] .= "                        <!-- /.panel-body -->"; 
		$data['content'] .= "                    </div>"; 
		$data['content'] .= "                    <!-- /.panel -->"; 
		$data['content'] .= "                </div>"; 
		$data['content'] .= "                <!-- /.col-lg-12 -->"; 
		$data['content'] .= "            </div>";


		$data['content'] .= "</div>";
		$data['content'] .= "</div>";
		$data['content'] .= "</div>";


        //$this->load->view('student/student_view3', $data);
        $this->load->view('student/schedule_form_view', $data);
        //$this->load->view('student/student_schedule_view', $data);
	}
    
    function insert_schedule()
    {

    	$this->debate_model->insert_student_schedule();
    	redirect('/student/submitted_times','refresh');
    }
	
	function submit_success()
	{
		$data['content'] = "<div class='alert alert-success alert-dismissable'>";
		$data['content'] .= "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
		$data['content'] .= "Thank you! Your times have been submitted.";
		$data['content'] .= "</div>";

		$this->load->view('student/student_schedule_view', $data);
	}

	//page where student can view basic account information
	function account()
	{
		$this->studentmenu->show_menu();

		$username = $this->session->userdata('username');
		$data['student'] = $this->debate_model->get_student($username); 

		$fname = $data['student']['fname'];
		$lname = $data['student']['lname'];
		$name = $fname.' '.$lname;


		if(empty($data['student']['major']))
		{
			$major = '<i>not provided</i>';
		}
		else
		{
			$major = $data['student']['major'];	
		}

		if(empty($data['student']['class']))
		{
			$class = '<i>not provided</i>';
		}
		else
		{
			$class = $data['student']['class'];	
		}
		
		$email = $data['student']['email'];

		

		$data['content'] =	'	<div id="page-wrapper">';
		$data['content'] .= '            <div class="row">';
		$data['content'] .= '                <div class="col-lg-12">';
		$data['content'] .= '                    <h1 class="page-header">';
		$data['content'] .= 						$name;
		$data['content'] .= '					</h1>';

		$data['content'] .= "                            <div class=\"table-responsive\">\n"; 
        $data['content'] .= "                                <table class=\"table\">\n"; 
        $data['content'] .= "                                        <tr>\n"; 
        $data['content'] .= "                                            <th>Major:</th>\n"; 
        $data['content'] .= "                                            <td>";
        $data['content'] .= 												$major;
        $data['content'] .= "											</td>\n";
        $data['content'] .= "                                        </tr>\n";

        $data['content'] .= "                                        <tr>\n"; 
        $data['content'] .= "                                            <th>Class:</th>\n"; 
        $data['content'] .= "                                            <td>";
        $data['content'] .= 												$class;
        $data['content'] .= "											</td>\n";
        $data['content'] .= "                                        </tr>\n";

		$data['content'] .= "                                        <tr>\n"; 
		$data['content'] .= "                                            <th>Email:</th>\n"; 
		$data['content'] .= "                                            <td>";
		$data['content'] .= 												$email;
		$data['content'] .= "											</td>\n";
		$data['content'] .= "                                        </tr>\n";
        $data['content'] .= "                                </table>\n";

        $data['content'] .= '<button onclick="window.location=\'./edit_account\';" type="button" class="btn btn-primary">Edit Above Information</button><br />';
        $data['content'] .= '<br /><button onclick="window.location=\'./change_password\';" type="button" class="btn btn-danger">Change Password</button>';



		$data['content'] .= '                </div>';
		$data['content'] .= '                <!-- /.col-lg-12 -->';
		$data['content'] .= '            </div>';
		$data['content'] .= '            <!-- /.row -->';
		$data['content'] .= '        </div>';
		$data['content'] .= '        <!-- /#page-wrapper -->';

		$data['content'] .= '    </div>';

		//$data['content'] = 'hello student';
        $this->load->view('student/student_view3', $data);
        //$this->load->view('student/student_view', $data);
	}

	function edit_account()
	{
		$this->studentmenu->show_menu();

		//echo base_url();

		$username = $this->session->userdata('username');
		$data['student'] = $this->debate_model->get_student($username); 

		$fname = $data['student']['fname'];
		$lname = $data['student']['lname'];
		$name = $fname.' '.$lname;

		// $data['content'] =	'	<div id="page-wrapper">';
		// $data['content'] .= '            <div class="row">';
		// $data['content'] .= '                <div class="col-lg-12">';
		// $data['content'] .= '                    <h1 class="page-header">';
		// $data['content'] .= '						Edit Account';
		// $data['content'] .= '					</h1>';



		$data['content'] = '<div id="page-wrapper">';
		$data['content'] .='            <div class="row">';
		$data['content'] .='                <div class="col-lg-12">';
		$data['content'] .='                    <h1 class="page-header">Edit Account</h1>';
		$data['content'] .='                </div>';
		$data['content'] .='                <!-- /.col-lg-12 -->';
		$data['content'] .='            </div>';
		$data['content'] .='            <!-- /.row -->';
		$data['content'] .='            <div class="row">';
		$data['content'] .='                <div class="col-lg-12">';
		$data['content'] .='                    <div class="panel panel-default">';
		$data['content'] .='                        <div class="panel-heading">';
		$data['content'] .='                            Please complete the form below';
		$data['content'] .='                        </div>';
		$data['content'] .='                        <div class="panel-body">';
		$data['content'] .='                            <div class="row">';
		$data['content'] .='                                <div class="col-lg-6">';

		//starting form
		//input box for Major
		//query database for classification
		//disabled form with email address inside
		//submit button
		//create new view with the jquery in it
		$data['content'] .='                        <div class="panel-body">';
		$data['content'] .='                            <div class="row">';
		$data['content'] .='                                <div class="col-lg-6">';
		$data['content'] .='        <form id="editAccount" role="form" action="./update_account" method="post">';
		$data['content'] .='                                        <div class="form-group">';
		$data['content'] .='                                            <label>Major</label>';
		$data['content'] .='                                            <input class="form-control" placeholder="ex: Computer Science" id="major" name="major">';
		$data['content'] .='                                        </div>';
		//classification
		$data['content'] .='<div class="form-group">';		
		$data['content'] .= '    <label>Class</label><br />'; 
		//$data['content'] .= '    <select id="classification" name="classification" class="input-large"><br />';
		//get list of classificaitons
		$data['classification'] = $this->debate_model->get_classifications();
		foreach($data['classification'] as $c)
		{

			$class_title = $c->class_title;
			$classID = $c->classID;

			$data['content'] .= "<div class='checkbox'>";
			$data['content'] .= "<label>";
			$data['content'] .= "<input type='checkbox' id='classification' name='classification' value='$classID'>$class_title";
			$data['content'] .= "</label>";
			$data['content'] .= "</div>";

			
			//$data['content'] .= "<option>$class_title</option><br />";
		}
		//$data['content'] .= '</select><br /><br />';
		$data['content'] .= '</div>';




		$data['content'] .='                                        <button type="submit" class="btn btn-success">Submit</button>';
		//$data['content'] .='                                        <button type="reset" class="btn btn-default">Reset</button>';
		$data['content'] .='                                    </form>';
		$data['content'] .='                                </div>';
		$data['content'] .='                                <!-- /.col-lg-6 (nested) -->';
		$data['content'] .='                            </div>';
		$data['content'] .='                            <!-- /.row (nested) -->';
		$data['content'] .='                        </div>';
		$data['content'] .='                        <!-- /.panel-body -->';
		$data['content'] .='                    </div>';
		$data['content'] .='                    <!-- /.panel -->';
		$data['content'] .='                </div>';
		$data['content'] .='                <!-- /.col-lg-12 -->';
		$data['content'] .='            </div>';
		$data['content'] .='            <!-- /.row -->';
		$data['content'] .='        </div>';
		$data['content'] .='        <!-- /#page-wrapper -->';


		//$this->load->view('admin/form_tester', $data);
		$this->load->view('student/account_form', $data);
	}

	function update_account()
	{
		$username = $this->session->userdata('username');
		$this->debate_model->update_student($username);

		redirect('student/account/','refresh');
	}

	//Since I don't know how to do this using ajax and jquery, I'm setting values in check_pw() to check
	//	if the user entered the correct current password.
	//	2: the user has not submitted a password yet; show form asking user to enter current password
	//	1: the user entered the correct current password; show form asking for new password
	//	0: the user entered the wrong current password; tell user password is incorrect, show form to try again
	function change_password()
	{
		$this->studentmenu->show_menu();

		$flag = $this->check_pw();

		//since I don't know how to do this using ajax and jquery, I'm setting flags to check
		//	if the user entered the correct current password
		if($flag == 2)
		{

			$username = $this->session->userdata('username');
			$data['student'] = $this->debate_model->get_student($username); 

			$fname = $data['student']['fname'];
			$lname = $data['student']['lname'];
			$name = $fname.' '.$lname;



			$data['content'] = '<div id="page-wrapper">';
			$data['content'] .='            <div class="row">';
			$data['content'] .='                <div class="col-lg-12">';
			$data['content'] .='                    <h1 class="page-header">Change Password</h1>';
			$data['content'] .='                </div>';
			$data['content'] .='                <!-- /.col-lg-12 -->';
			$data['content'] .='            </div>';
			$data['content'] .='            <!-- /.row -->';
			$data['content'] .='            <div class="row">';
			$data['content'] .='                <div class="col-lg-12">';
			$data['content'] .='                    <div class="panel panel-default">';
			$data['content'] .='                        <div class="panel-heading">';
			$data['content'] .='                            ';
			$data['content'] .='                        </div>';
			$data['content'] .='                        <div class="panel-body">';
			$data['content'] .='                            <div class="row">';
			$data['content'] .='                                <div class="col-lg-6">';

			//starting form
			$data['content'] .='                        <div class="panel-body">';
			$data['content'] .='                            <div class="row">';
			$data['content'] .='                                <div class="col-lg-6">';
			$data['content'] .='        <form id="editAccount" role="form" action="./change_password" method="post">';
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Current Password</label>';
			$data['content'] .='                                            <input type="password" class="form-control" id="curr_pw" name="curr_pw">';
			$data['content'] .='                                        </div>'; //form-group

			$data['content'] .='                                        <button type="submit" class="btn btn-default">Next</button>';
			//$data['content'] .='                                        <button type="reset" class="btn btn-default">Reset</button>';
			$data['content'] .='                                    </form>';
			$data['content'] .='                                </div>';
			$data['content'] .='                                <!-- /.col-lg-6 (nested) -->';
			$data['content'] .='                            </div>';
			$data['content'] .='                            <!-- /.row (nested) -->';
			$data['content'] .='                        </div>';
			$data['content'] .='                        <!-- /.panel-body -->';
			$data['content'] .='                    </div>';
			$data['content'] .='                    <!-- /.panel -->';
			$data['content'] .='                </div>';
			$data['content'] .='                <!-- /.col-lg-12 -->';
			$data['content'] .='            </div>';
			$data['content'] .='            <!-- /.row -->';
			$data['content'] .='        </div>';
			$data['content'] .='        <!-- /#page-wrapper -->';


			//$this->load->view('admin/form_tester', $data);
			$this->load->view('student/account_form', $data);
		} //end flag = 2

		elseif($flag == 1)
		{

			$username = $this->session->userdata('username');
			$data['student'] = $this->debate_model->get_student($username); 

			$fname = $data['student']['fname'];
			$lname = $data['student']['lname'];
			$name = $fname.' '.$lname;



			$data['content'] = '<div id="page-wrapper">';
			$data['content'] .='            <div class="row">';
			$data['content'] .='                <div class="col-lg-12">';
			$data['content'] .='                    <h1 class="page-header">Change Password</h1>';
			$data['content'] .='                </div>';
			$data['content'] .='                <!-- /.col-lg-12 -->';
			$data['content'] .='            </div>';
			$data['content'] .='            <!-- /.row -->';
			$data['content'] .='            <div class="row">';
			$data['content'] .='                <div class="col-lg-12">';
			$data['content'] .='                    <div class="panel panel-default">';
			$data['content'] .='                        <div class="panel-heading">';
			$data['content'] .='                            ';
			$data['content'] .='                        </div>';
			$data['content'] .='                        <div class="panel-body">';
			$data['content'] .='                            <div class="row">';
			$data['content'] .='                                <div class="col-lg-6">';

			//starting form
			$data['content'] .='                        <div class="panel-body">';
			$data['content'] .='                            <div class="row">';
			$data['content'] .='                                <div class="col-lg-6">';
			$data['content'] .='        <form id="editAccount" role="form" action="./set_new_pw" method="post">';
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>New Password</label>';
			$data['content'] .='                                            <input type="password" class="form-control" id="new_pw" name="new_pw">';
			$data['content'] .='                                        </div>'; //form-group
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Retype Password</label>';
			$data['content'] .='                                            <input type="password" class="form-control" id="confirm" name="confirm">';
			$data['content'] .='                                        </div>'; //form-group

			$data['content'] .='                                        <button type="submit" class="btn btn-success">Submit</button>';
			//$data['content'] .='                                        <button type="reset" class="btn btn-default">Reset</button>';
			$data['content'] .='                                    </form>';
			$data['content'] .='                                </div>';
			$data['content'] .='                                <!-- /.col-lg-6 (nested) -->';
			$data['content'] .='                            </div>';
			$data['content'] .='                            <!-- /.row (nested) -->';
			$data['content'] .='                        </div>';
			$data['content'] .='                        <!-- /.panel-body -->';
			$data['content'] .='                    </div>';
			$data['content'] .='                    <!-- /.panel -->';
			$data['content'] .='                </div>';
			$data['content'] .='                <!-- /.col-lg-12 -->';
			$data['content'] .='            </div>';
			$data['content'] .='            <!-- /.row -->';
			$data['content'] .='        </div>';
			$data['content'] .='        <!-- /#page-wrapper -->';


			//$this->load->view('admin/form_tester', $data);
			$this->load->view('student/account_form', $data);
		} //end flag = 1

		elseif($flag == 0)
		{

			$username = $this->session->userdata('username');
			$data['student'] = $this->debate_model->get_student($username); 

			$fname = $data['student']['fname'];
			$lname = $data['student']['lname'];
			$name = $fname.' '.$lname;



			$data['content'] = '<div id="page-wrapper">';
			$data['content'] .='            <div class="row">';
			$data['content'] .='                <div class="col-lg-12">';
			$data['content'] .='                    <h1 class="page-header">Change Password</h1>';
			$data['content'] .='                </div>';
			$data['content'] .='                <!-- /.col-lg-12 -->';
			$data['content'] .='            </div>';
			$data['content'] .='            <!-- /.row -->';
			$data['content'] .='            <div class="row">';
			$data['content'] .='                <div class="col-lg-12">';
			$data['content'] .='                    <div class="panel panel-default">';
			$data['content'] .='                        <div class="panel-heading">';
			$data['content'] .='                            <font color="red">Incorect password. Try again.</font>';
			$data['content'] .='                        </div>';
			$data['content'] .='                        <div class="panel-body">';
			$data['content'] .='                            <div class="row">';
			$data['content'] .='                                <div class="col-lg-6">';

			//starting form
			$data['content'] .='                        <div class="panel-body">';
			$data['content'] .='                            <div class="row">';
			$data['content'] .='                                <div class="col-lg-6">';
			$data['content'] .='        <form id="editAccount" role="form" action="./change_password" method="post">';
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Current Password</label>';
			$data['content'] .='                                            <input type="password" class="form-control" id="curr_pw" name="curr_pw">';
			$data['content'] .='                                        </div>'; //form-group

			$data['content'] .='                                        <button type="submit" class="btn btn-default">Next</button>';
			//$data['content'] .='                                        <button type="reset" class="btn btn-default">Reset</button>';
			$data['content'] .='                                    </form>';
			$data['content'] .='                                </div>';
			$data['content'] .='                                <!-- /.col-lg-6 (nested) -->';
			$data['content'] .='                            </div>';
			$data['content'] .='                            <!-- /.row (nested) -->';
			$data['content'] .='                        </div>';
			$data['content'] .='                        <!-- /.panel-body -->';
			$data['content'] .='                    </div>';
			$data['content'] .='                    <!-- /.panel -->';
			$data['content'] .='                </div>';
			$data['content'] .='                <!-- /.col-lg-12 -->';
			$data['content'] .='            </div>';
			$data['content'] .='            <!-- /.row -->';
			$data['content'] .='        </div>';
			$data['content'] .='        <!-- /#page-wrapper -->';


			//$this->load->view('admin/form_tester', $data);
			$this->load->view('student/account_form', $data);
		} //end flag = 0


	}// end change_password($flag)


	//function used to check if the password is correct
	function check_pw()
	{
		// echo '<pre>';
		// echo var_dump($_POST);
		// echo '</pre>';

		if(empty($_POST))
		{
			return 2;
		}
		else
		{
			$username = $this->session->userdata('username');
			$correct = $this->debate_model->check_pw($username);
			//$theInput = $this->input->post('curr_pw');
			if($correct)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}

	}

	//function for inserting the new password into the db
	function set_new_pw()
	{
		$username = $this->session->userdata('username');
		$this->debate_model->update_pw($username);

		redirect('student/account/','refresh');
		
	}

	function submitted_times()
	{
		$this->studentmenu->show_menu();

		$data['content'] =	'	<div id="page-wrapper">';
		$data['content'] .= '            <div class="row">';
		$data['content'] .= '                <div class="col-lg-12">';
		$data['content'] .= '                    <h1 class="page-header">Submitted Times</h1>';
		$data['content'] .= '                </div>';
		$data['content'] .= '                <!-- /.col-lg-12 -->';
		$data['content'] .= '            </div>';
		$data['content'] .= '            <!-- /.row -->';
		$data['content'] .='            <div class="row">';
		$data['content'] .='                <div class="col-lg-12">';
		$data['content'] .='                        <div class="panel-body">';
		$data['content'] .='                            <div class="row">';
		$data['content'] .='                                <div class="col-lg-6">';
		$data['content'] .= "                        <div class=\"panel-body\">\n";
		$data['content'] .='						The following is a list of times you are available to debate:<br /><br />';
        
        $data['content'] .= "                            <div class=\"table-responsive\">\n"; 
        $data['content'] .= "                                <table class=\"table\">\n"; 
        $data['content'] .= "                                    <thead>\n"; 
        $data['content'] .= "                                        <tr>\n"; 
        $data['content'] .= "                                            <th>Date</th>\n"; 
        $data['content'] .= "                                            <th></th>\n"; 
        $data['content'] .= "                                            <th>Time</th>\n";
        $data['content'] .= "                                        </tr>\n"; 
        $data['content'] .= "                                    </thead>\n";
        $data['content'] .= "                                    <tbody>\n";


		$data['times'] = $this->debate_model->student_submitted_times();
		foreach($data['times'] as $d)
		{
			$date = $d->date;
			$time = $d->start;

			$dateAndTime = $date.' at '.$time;

			$data['content'] .= "                                        <tr>\n"; 
            $data['content'] .= "                                            <td>$date</td>\n"; 
            $data['content'] .= "                                            <td>at</td>\n"; 
            $data['content'] .= "                                            <td>$time</td>\n"; 
            $data['content'] .= "                                        </tr>\n";

		}


		$data['content'] .= "                                    </tbody>\n"; 
		$data['content'] .= "                                </table>\n";
		$data['content'] .= '	</div>'; //table-responsive
		$data['content'] .= '        </div>';//panel-body
		$data['content'] .= '    </div>';//col-lg-6
		$data['content'] .= '    </div>';//row
		$data['content'] .= '    </div>';//panel-body
		$data['content'] .= '    </div>';//col-lg-12
		$data['content'] .= '    </div>';//row
		$data['content'] .= '    </div>';//page-wrapper



		//$data['content'] = 'hello student';
        $this->load->view('student/student_view3', $data);


	}



	
	//checks to see if user has permission to view page
	function is_logged_in()
    {
        $is_logged_in = $this->session->userdata('is_logged_in');
        $acc_level = $this->session->userdata('acc_level');
        
        if(!isset($is_logged_in) || ($is_logged_in != true) || ($acc_level != 3) )
        {
            echo 'You don\'t have permission to access this page. <a href="/~yuwah/SeniorProject/index.php/login2">Login</a>';
            die();
        }
    }
	
	
	
	
	
	





}
