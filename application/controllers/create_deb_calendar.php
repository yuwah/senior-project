<?php
class Create_deb_calendar extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('debate_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->is_logged_in();
	}
	
	public function index()
	{   
        $this->load->view('admin/create_deb_calendar_view');
	}
    
    public function insert_cal_dates()
    {
        $start_date = $this->input->post('start');
        $end_date = $this->input->post('end');
        
        //$this->debate_model->create_deb_calendar($start_date, $end_date);
        $this->debate_model->create_deb_calendar_test();
        redirect('/create_deb_calendar','refresh');
    }
	

	function is_logged_in()
    {
        $is_logged_in = $this->session->userdata('is_logged_in');
        
        if(!isset($is_logged_in) || $is_logged_in != true)
        {
            echo 'You don\'t have permission to access this page. <a href="./login2">Login</a>';
            die();
        }
    }
    
	
	
	
	
	





}
