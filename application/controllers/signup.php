<?php
class Signup extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('debate_model');
		$this->load->model('membership_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
        //$this->is_logged_in();
        //$this->load->library('adminMenu');
	}


	//Since I don't know how to do this using ajax and jquery, I'm setting values in check_username() to check
	//	if the user entered a username that already exists.
	//	2: the user hasn't entered anything yet; show default signup form
	//	1: the username already exists; show form to try again
	//	0: the username is not being used; submit data into db
	function index()
	{
		$flag = $this->check_username();

		$data['title'] = 'Student Sign Up';


		if($flag == 2)
		{
			$data['content'] ='        <form id="registrationForm" role="form" action="signup" method="post">';
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>First name</label>';
			$data['content'] .='                                            <input class="form-control" placeholder="ex: John" id="fname" name="fname">';
			$data['content'] .='                                        </div>';
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Last name</label>';
			$data['content'] .='                                            <input class="form-control" placeholder="ex: Smith" id="lname" name="lname">';
			$data['content'] .='                                        </div>';
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Username</label>';
			$data['content'] .='                                            <input class="form-control" placeholder="ex: jsmith" id="username" name="username">';
			$data['content'] .='                                        </div>';
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Email</label>';
			$data['content'] .='                                            <input class="form-control" placeholder="ex: jsmith@stedwards.edu" id="email" name="email" type="email">';
			$data['content'] .='                                        </div>';
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Retype Email</label>';
			$data['content'] .='                                            <input class="form-control" id="confirmEmail" name="confirmEmail" type="email">';
			$data['content'] .='                                        </div>';

			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Password</label>';
			$data['content'] .='                                            <input type="password" class="form-control" id="password" name="password">';
			$data['content'] .='                                        </div>'; //form-group
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Retype Password</label>';
			$data['content'] .='                                            <input type="password" class="form-control" id="confirmPassword" name="confirmPassword">';
			$data['content'] .='                                        </div>'; //form-group

			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Major</label>';
			$data['content'] .='                                            <input class="form-control" placeholder="ex: Computer Science" id="major" name="major">';
			$data['content'] .='                                        </div>';
			//classification
			$data['content'] .='<div class="form-group">';		
			$data['content'] .= '    <label>Class</label><br />'; 
			//$data['content'] .= '    <select id="classification" name="classification" class="input-large"><br />';
			//get list of classificaitons
			$data['classification'] = $this->debate_model->get_classifications();
			foreach($data['classification'] as $c)
			{

				$class_title = $c->class_title;
				$classID = $c->classID;

				$data['content'] .= "<div class='radio'>";
				$data['content'] .= "<label>";
				$data['content'] .= "<input type='radio' id='classification' name='classification' value='$classID'>$class_title";
				$data['content'] .= "</label>";
				$data['content'] .= "</div>";

				
				//$data['content'] .= "<option>$class_title</option><br />";
			}
			//$data['content'] .= '</select><br /><br />';
			$data['content'] .= '</div>';

			$data['content'] .='                                        <button type="submit" class="btn btn-default">Submit</button>';

			$this->load->view('signup_view', $data);
		}//end flag == 2

		if($flag == 1)
		{
			$data['content'] ='        <form id="registrationForm" role="form" action="signup" method="post">';
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>First name</label>';
			$data['content'] .='                                            <input class="form-control" placeholder="ex: John" id="fname" name="fname">';
			$data['content'] .='                                        </div>';
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Last name</label>';
			$data['content'] .='                                            <input class="form-control" placeholder="ex: Smith" id="lname" name="lname">';
			$data['content'] .='                                        </div>';
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label><font color="red">Username already exists</font></label>';
			$data['content'] .='                                            <input class="form-control" placeholder="ex: jsmith" id="username" name="username">';
			$data['content'] .='                                        </div>';
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Email</label>';
			$data['content'] .='                                            <input class="form-control" placeholder="ex: jsmith@stedwards.edu" id="email" name="email" type="email">';
			$data['content'] .='                                        </div>';
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Retype Email</label>';
			$data['content'] .='                                            <input class="form-control" id="confirmEmail" name="confirmEmail" type="email">';
			$data['content'] .='                                        </div>';

			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Password</label>';
			$data['content'] .='                                            <input type="password" class="form-control" id="password" name="password">';
			$data['content'] .='                                        </div>'; //form-group
			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Retype Password</label>';
			$data['content'] .='                                            <input type="password" class="form-control" id="confirmPassword" name="confirmPassword">';
			$data['content'] .='                                        </div>'; //form-group

			$data['content'] .='                                        <div class="form-group">';
			$data['content'] .='                                            <label>Major</label>';
			$data['content'] .='                                            <input class="form-control" placeholder="ex: Computer Science" id="major" name="major">';
			$data['content'] .='                                        </div>';
			//classification
			$data['content'] .='<div class="form-group">';		
			$data['content'] .= '    <label>Class</label><br />'; 
			//$data['content'] .= '    <select id="classification" name="classification" class="input-large"><br />';
			//get list of classificaitons
			$data['classification'] = $this->debate_model->get_classifications();
			foreach($data['classification'] as $c)
			{

				$class_title = $c->class_title;
				$classID = $c->classID;

				$data['content'] .= "<div class='radio'>";
				$data['content'] .= "<label>";
				$data['content'] .= "<input type='radio' id='classification' name='classification' value='$classID'>$class_title";
				$data['content'] .= "</label>";
				$data['content'] .= "</div>";

				
				//$data['content'] .= "<option>$class_title</option><br />";
			}
			//$data['content'] .= '</select><br /><br />';
			$data['content'] .= '</div>';
			
			$data['content'] .='                                        <button type="submit" class="btn btn-default">Submit</button>';

			$this->load->view('signup_view', $data);
		}//end flag == 1

		// if($flag == 0)
		// {
		// 	$data['content'] ='        <form id="registrationForm" role="form" action="./insert_participant" method="post">';
		// 	$data['content'] .='                                        <div class="form-group">';
		// 	$data['content'] .='                                            <label>First name</label>';
		// 	$data['content'] .='                                            <input class="form-control" placeholder="ex: John" id="fname" name="fname">';
		// 	$data['content'] .='                                        </div>';
		// 	$data['content'] .='                                        <div class="form-group">';
		// 	$data['content'] .='                                            <label>Last name</label>';
		// 	$data['content'] .='                                            <input class="form-control" placeholder="ex: Smith" id="lname" name="lname">';
		// 	$data['content'] .='                                        </div>';
		// 	$data['content'] .='                                        <div class="form-group">';
		// 	$data['content'] .='                                            <label>Username</label>';
		// 	$data['content'] .='                                            <input class="form-control" placeholder="ex: jsmith" id="username" name="username">';
		// 	$data['content'] .='                                        </div>';
		// 	$data['content'] .='                                        <div class="form-group">';
		// 	$data['content'] .='                                            <label>Email</label>';
		// 	$data['content'] .='                                            <input class="form-control" placeholder="ex: jsmith@stedwards.edu" id="email" name="email" type="email">';
		// 	$data['content'] .='                                        </div>';
		// 	$data['content'] .='                                        <div class="form-group">';
		// 	$data['content'] .='                                            <label>Retype Email</label>';
		// 	$data['content'] .='                                            <input class="form-control" id="confirmEmail" name="confirmEmail" type="email">';
		// 	$data['content'] .='                                        </div>';

		// 	$data['content'] .='                                        <div class="form-group">';
		// 	$data['content'] .='                                            <label>Password</label>';
		// 	$data['content'] .='                                            <input type="password" class="form-control" id="password" name="password">';
		// 	$data['content'] .='                                        </div>'; //form-group
		// 	$data['content'] .='                                        <div class="form-group">';
		// 	$data['content'] .='                                            <label>Retype Password</label>';
		// 	$data['content'] .='                                            <input type="password" class="form-control" id="confirmPassword" name="confirmPassword">';
		// 	$data['content'] .='                                        </div>'; //form-group

		// 	$data['content'] .='                                        <div class="form-group">';
		// 	$data['content'] .='                                            <label>Select One</label>';
		// 	$data['content'] .='                                            <div class="radio">';
		// 	$data['content'] .='                                                <label>';
		// 	$data['content'] .='                                                    <input type="radio" name="parttype" value="1">Student';
		// 	$data['content'] .='                                                </label>';
		// 	$data['content'] .='                                            </div>';
		// 	$data['content'] .='                                            <div class="radio">';
		// 	$data['content'] .='                                                <label>';
		// 	$data['content'] .='                                                    <input type="radio" name="parttype" value="2" disabled>Judge';
		// 	$data['content'] .='                                                </label>';
		// 	$data['content'] .='                                            </div>';
		// 	$data['content'] .='                                        </div>';
		// 	$data['content'] .='                                        ';
		// 	$data['content'] .='                                        <button type="submit" class="btn btn-default">Submit</button>';

		// 	$this->load->view('signup_view', $data);
		// }
	}


	function check_username()
	{
		// echo '<pre>';
		// echo var_dump($_POST);
		// echo '</pre>';

		if(empty($_POST))
		{
			return 2;
		}
		else
		{
			$username = $this->input->post('username');
			//$this->membership_model->check_user_exists($username);
			$exists = $this->membership_model->check_user_exists($username);
			if($exists)
			{
				return 1;
			}
			else
			{
				// echo '<pre>';
				// echo var_dump($_POST);
				// echo '</pre>';
				//echo 'Will send the following to model to insert into database<br />';
				$this->membership_model->insert_participant_signup();
				redirect('login2', 'refresh');
			}
		}
	}

	function create_account()
	{
		echo 'create account';
	}
















}//end class Signup