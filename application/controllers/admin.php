<?php
class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('debate_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
        $this->is_logged_in();
        $this->load->library('adminMenu');
	}
	
	//function for admin home page
	public function index()
	{
		$this->adminmenu->show_menu();
  

		$data['content'] = '        <div id="page-wrapper">';
		$data['content'] .= '            <div class="row">';
		$data['content'] .= '                <div class="col-lg-12">';
		$data['content'] .= '                    <h1 class="page-header">Welcome!</h1>';
		$data['content'] .= '                </div>'; //col-lg-12
		$data['content'] .= '            </div>'; //row
		$data['content'] .= '        </div>'; //page-wrapper
        
        $data['content'] .= '</div>'; //end wrapper div

        $this->load->view('admin/admin_view3', $data);
  //       $this->load->view('admin/admin_view2', $data);
        //$this->load->view('admin/admin_view', $data);
	}
	
	//form for creating a new student or judge account
	//need to add function to insert the participant
	function add_participant()
	{
		$this->adminmenu->show_menu();

        $data['content'] = '<div id="page-wrapper">';
		$data['content'] .='            <div class="row">';
		$data['content'] .='                <div class="col-lg-12">';
		$data['content'] .='                    <h1 class="page-header">Create New Participant</h1>';
		$data['content'] .='                </div>';
		$data['content'] .='                <!-- /.col-lg-12 -->';
		$data['content'] .='            </div>';
		$data['content'] .='            <!-- /.row -->';
		$data['content'] .='            <div class="row">';
		$data['content'] .='                <div class="col-lg-12">';
		$data['content'] .='                    <div class="panel panel-default">';
		$data['content'] .='                        <div class="panel-heading">';
		$data['content'] .='                            Please complete the form below';
		$data['content'] .='                        </div>';
		$data['content'] .='                        <div class="panel-body">';
		$data['content'] .='                            <div class="row">';
		$data['content'] .='                                <div class="col-lg-6">';
		$data['content'] .='        <form id="registrationForm" role="form" action="./insert_participant" method="post">';
		$data['content'] .='                                        <div class="form-group">';
		$data['content'] .='                                            <label>First name</label>';
		$data['content'] .='                                            <input class="form-control" placeholder="ex: John" id="fname" name="fname">';
		$data['content'] .='                                        </div>';
		$data['content'] .='                                        <div class="form-group">';
		$data['content'] .='                                            <label>Last name</label>';
		$data['content'] .='                                            <input class="form-control" placeholder="ex: Smith" id="lname" name="lname">';
		$data['content'] .='                                        </div>';
		// $data['content'] .='                                        <div class="form-group">';
		// $data['content'] .='                                            <label>Username</label>';
		// $data['content'] .='                                            <input class="form-control" placeholder="ex: jsmith" id="username" name="username">';
		// $data['content'] .='                                        </div>';
		$data['content'] .='                                        <div class="form-group">';
		$data['content'] .='                                            <label>Email</label>';
		$data['content'] .='                                            <input class="form-control" placeholder="ex: jsmith@stedwards.edu" id="email" name="email" type="email">';
		$data['content'] .='                                        </div>';
		$data['content'] .='                                        <div class="form-group">';
		$data['content'] .='                                            <label>Participant Type</label>';
		$data['content'] .='                                            <div class="checkbox">';
		$data['content'] .='                                                <label>';
		$data['content'] .='                                                    <input type="checkbox" name="parttype" value="1">Student';
		$data['content'] .='                                                </label>';
		$data['content'] .='                                            </div>';
		$data['content'] .='                                            <div class="checkbox">';
		$data['content'] .='                                                <label>';
		$data['content'] .='                                                    <input type="checkbox" name="parttype" value="2">Judge';
		$data['content'] .='                                                </label>';
		$data['content'] .='                                            </div>';
		$data['content'] .='                                        </div>';
		$data['content'] .='                                        ';
		$data['content'] .='                                        <button type="submit" class="btn btn-default">Submit</button>';


		// $data['content'] .= '<button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Small modal</button>';

		// $data['content'] .= '<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">';
		// $data['content'] .= '  <div class="modal-dialog modal-sm">';
		// $data['content'] .= '    <div class="modal-content">';
		// $data['content'] .= '      Success! The user has been created.';
		// $data['content'] .= '    </div>';
		// $data['content'] .= '  </div>';
		// $data['content'] .= '</div>';



		$data['content'] .='                                        <button type="reset" class="btn btn-default">Reset</button>';
		$data['content'] .='                                    </form>';
		$data['content'] .='                                </div>';
		$data['content'] .='                                <!-- /.col-lg-6 (nested) -->';
		$data['content'] .='                            </div>';
		$data['content'] .='                            <!-- /.row (nested) -->';
		$data['content'] .='                        </div>';
		$data['content'] .='                        <!-- /.panel-body -->';
		$data['content'] .='                    </div>';
		$data['content'] .='                    <!-- /.panel -->';
		$data['content'] .='                </div>';
		$data['content'] .='                <!-- /.col-lg-12 -->';
		$data['content'] .='            </div>';
		$data['content'] .='            <!-- /.row -->';
		$data['content'] .='        </div>';
		$data['content'] .='        <!-- /#page-wrapper -->';

		//$this->load->view('admin/admin_view3', $data);
		$this->load->view('admin/form_tester', $data);
	}

	//The admin no longer creates participants, so the call to insert_participant() has been disabled
	public function insert_participant()
    {

		//$this->debate_model->insert_participant();

        redirect('admin/add_participant/','refresh');
    }

    //shows the list of dates that are in this round's calendar
    function view_calendar()
    {
    	$this->adminmenu->show_menu();

    	$data['dates'] = $this->debate_model->get_all_deb_days();
    	// echo '<pre>';
    	// echo var_dump($data['dates']);
    	// echo '</pre>';

    	$data['content'] = '  <div id="page-wrapper">';
		$data['content'] .= '            <div class="row">';
		$data['content'] .= '                <div class="col-lg-12">';
		$data['content'] .= '                    <h1 class="page-header">View Current Calendar</h1>';
		$data['content'] .= '                </div>'; //col-lg-12
		$data['content'] .= '            </div>'; //row

		$data['content'] .= '<div id="page=wrapper">';
		$data['content'] .= '<div class="row">'; 
		$data['content'] .= "                <div class='col-lg-12'>";

		$data['content'] .= "                    <div class='panel panel-default'>"; 
		$data['content'] .= "                        <div class='panel-heading'>"; 
		$data['content'] .= "                            These are the dates of this semester's debate tournament."; 
		$data['content'] .= "                        </div>";
		$data['content'] .= "                        <div class='panel-body'>"; 
		//$data['content'] .= "                            <div class='row'>"; 
		//$data['content'] .= "                                <div class='col-lg-6'>";

		$data['content'] .= "                            <div class=\"table-responsive\">\n"; 
        $data['content'] .= "                                <table class=\"table\">\n"; 
        $data['content'] .= "                                    <thead>\n"; 
        $data['content'] .= "                                        <tr>\n"; 
        $data['content'] .= "                                            <th>Date</th>\n"; 
        $data['content'] .= "                                            <th>Time</th>\n";
        $data['content'] .= "                                        </tr>\n"; 
        $data['content'] .= "                                    </thead>\n";
        $data['content'] .= "                                    <tbody>\n";

		foreach($data['dates'] as $d)
		{
			$data['content'] .= "						<tr>\n";
			$data['content'] .= "							<td>$d->date</td>";
			$data['content'] .= "							<td>8:30 AM - 8:30 PM</td>";
			$data['content'] .= "						<tr>\n";
			//$data['content'] .= $d->date.'<br />';
		}

		$data['content'] .= "                                    </tbody>\n"; 
		$data['content'] .= "                                </table>\n";
		//$data['content'] .= '   </div>';//col-lg-6
		//$data['content'] .= '   </div>';//row
		$data['content'] .= '   </div>';//panel-body
		$data['content'] .= '   </div>';//panel panel-default		

		$data['content'] .= '   </div>';//col-lg-12
		$data['content'] .= '   </div>';//row
		$data['content'] .= '   </div>';//page-wrapper


		$data['content'] .= '   </div>'; //page-wrapper
        
        $data['content'] .= '</div>'; //end wrapper div

        $this->load->view('admin/admin_view3', $data);
    }

	function create_deb_calendar()
	{
		$this->adminmenu->show_menu();

		$cal_empty = $this->debate_model->is_cal_empty();


        $data['content'] = '<div id="page-wrapper">';
		$data['content'] .= '            <div class="row">';
		$data['content'] .= '                <div class="col-lg-12">';
		$data['content'] .= '                    <h1 class="page-header">Create A New Debate Calendar</h1>';
		$data['content'] .= '                </div>';
		$data['content'] .= '                <!-- /.col-lg-12 -->';
		$data['content'] .= '            </div>';
		$data['content'] .= '            <!-- /.row -->';

		$data['content'] .= '            <!--**************************************************************************************';
		$data['content'] .= '            //Date picker things';
		$data['content'] .= '            //************************************************************************************** -->';

		if($cal_empty)
		{

			$data['content'] .= '<div id="page=wrapper">';
			$data['content'] .= '<div class="row">'; 
			$data['content'] .= "                <div class='col-lg-12'>";

			$data['content'] .= "                    <div class='panel panel-default'>"; 
			$data['content'] .= "                        <div class='panel-heading'>"; 
			$data['content'] .= "                            Select span of dates the tournament will span. <b>This form will completely replace any previously submitted calendar.</b>"; 
			$data['content'] .= "                        </div>";
			$data['content'] .= "                        <div class='panel-body'>"; 
			$data['content'] .= "                            <div class='row'>"; 
			$data['content'] .= "                                <div class='col-lg-6'>";

			$data['content'] .= '            <div class="container">';
			$data['content'] .= '                <div class="hero-unit">';
			$data['content'] .= '                    <div class="input-daterange" id="datepicker" >';
			$data['content'] .= '                        <form role="form" action="./insert_cal_dates" method="post">';

			$data['content'] .= '                            <div class="form-group">';
			$data['content'] .= '                                <input type="text" class="input-small" name="start" />';
			$data['content'] .= '                                <span class="add-on" style="vertical-align: top;height:20px">to</span>';
			$data['content'] .= '                                <input type="text" class="input-small" name="end" />';
			$data['content'] .= '                            </div>';

			$data['content'] .= '                            <button type="submit" class="btn btn-default">Submit</button>';
			$data['content'] .= '                            <button type="reset" class="btn btn-default">Reset</button>';
			$data['content'] .= '                        </form>';

			$data['content'] .= '                    </div>';//input-daterange
			$data['content'] .= '                </div>';//hero-unit
			$data['content'] .= '            </div>';//container

			$data['content'] .= '</div>';//col-lg-6
			$data['content'] .= '</div>';//row
			$data['content'] .= '</div>';//panel-body
			$data['content'] .= '</div>';//panel panel-default

					$data['content'] .= "</div>";//col-lg-12
			$data['content'] .= "</div>";//row
			$data['content'] .= "</div>";//page-wrapper
		}
		else
		{
			$data['content'] .= '<div id="page=wrapper">';
			$data['content'] .= '<div class="row">'; 
			$data['content'] .= "                <div class='col-lg-12'>";

			$data['content'] .= "                    <div class='panel panel-default'>"; 
			$data['content'] .= "                        <div class='panel-body'>"; 
			$data['content'] .= "                            <div class='row'>"; 
			$data['content'] .= "                                <div class='col-lg-6'>";

			$data['content'] .= "You've already created a calendar. To delete the old calendar and create a new one, click 'Restart' on the side menu.";

			$data['content'] .= '								</div>';//col-lg-6
			$data['content'] .= '							</div>';//row
			$data['content'] .= '						</div>';//panel-body
			$data['content'] .= '					</div>';//panel panel-default

					$data['content'] .= "</div>";//col-lg-12
			$data['content'] .= "</div>";//row
			$data['content'] .= "</div>";//page-wrapper
		}

		$data['content'] .= '        </div>';//page-wrapper
		$data['content'] .= '        <!-- /#page-wrapper -->';


		$this->load->view('admin/create_deb_calendar_view', $data);
	}

	function insert_cal_dates()
    {
    	$this->adminmenu->show_menu();

        $start_date = $this->input->post('start');
        $end_date = $this->input->post('end');

		//converts start & end strings to unix time stamps
        $unix_start = strtotime($start_date);
        $unix_end = strtotime($end_date);

        //now I can edit the format of the time stamp to appear as "Wed August 20, 2014"
        $converted_start = date("D F j, Y", $unix_start);
        $converted_end = date("D F j, Y", $unix_end);

        // echo '<pre>';
        // echo var_dump($converted_start);
        // echo '</pre>';
        
        // echo '<pre>';
        // echo var_dump($converted_end);
        // echo '</pre>';
        //$this->debate_model->create_deb_calendar($start_date, $end_date);
        $this->debate_model->create_deb_calendar_test($start_date, $end_date);
        redirect('/admin/view_calendar','refresh');
    }

	function schedules()
	{
		$this->adminmenu->show_menu();

		$data['content'] = " ";
		$data['content'] .= "<div id='page-wrapper'>";
		$data['content'] .= "    <div class='row'>";
		$data['content'] .= "		<div class='col-lg-12'>";
        $data['content'] .=  "           <h1 class='page-header'></h1>";
        $data['content'] .=  "       </div>";

		$data['content'] .= '<div class="row">'; 
		$data['content'] .= "                <div class='col-lg-12'>";
        $data['content'] .= "<form role='form' action='./search' method='post'>\n";
          $data['content'] .= "<fieldset>\n"; 
          $data['content'] .= "\n"; 
          $data['content'] .= "<!-- Form Name -->\n"; 
          $data['content'] .= "<legend>Select which participants you would like to view below:</legend>\n"; 
          $data['content'] .= "\n"; 
          $data['content'] .= "<!-- Select Basic -->\n"; 
          $data['content'] .= "<div class=\"control-group\">\n"; 
          $data['content'] .= "  <label class=\"control-label\" for=\"parttype\">Select all </label>\n"; 
          $data['content'] .= "\n"; 
          $data['content'] .= "    <select id=\"parttype\" name=\"parttype\" class=\"input-large\">\n"; 
          $data['content'] .= "      <option value = '1'>Students</option>\n"; 
          $data['content'] .= "      <option value = '2'>Judges</option>\n";
          $data['content'] .= "      <option value = '3'>Students and Judges</option>\n";
          $data['content'] .= "    </select>\n"; 
          $data['content'] .= "  \n"; 
          $data['content'] .= "  <label class=\"control-label\" for=\"day\"> available on </label>\n"; 
          $data['content'] .= "    <select id=\"day\" name=\"day\" class=\"input-large\">\n";

          //gets all the dates from the database & puts them in a dropdown
          $data['days'] = $this->debate_model->get_all_deb_days();
          foreach($data['days'] as $d)
          {
                $day = $d->date;
                //$theday = date("D F j, Y", $day_unix);
                $data['content'] .= "<option>$day</option>\n";
          }

          $data['content'] .= "    </select>\n"; 
          $data['content'] .= "    \n";

          $data['content'] .= "    <label class=\"control-label\" for=\"time\"> at </label>\n"; 
          $data['content'] .= "    <select id=\"time\" name=\"time\" class=\"input-large\">\n"; 

          //get all the times from the database & put them in a dropdown
          $data['times'] = $this->debate_model->get_times();
          foreach($data['times'] as $t)
          {
          		$time = $t->start;
          		$data['content'] .= "<option>$time</option>\n";
          }

          
          $data['content'] .= "    </select>\n"; 
          $data['content'] .= "  \n"; 
          $data['content'] .= "      \n"; 
          $data['content'] .= "  </div>\n"; //control-group
          $data['content'] .= "</div>\n"; //col-lg-12
          $data['content'] .= "\n"; 
          $data['content'] .= "    \n"; 
          $data['content'] .= "<!-- Button (Double) -->\n"; 
          $data['content'] .= "<div class=\"control-group\">\n"; 
          $data['content'] .= "  <label class=\"control-label\" for=\"button1id\"></label>\n"; 
          $data['content'] .= "  <div class=\"controls\">\n"; 
          $data['content'] .= "    <button id=\"button1id\" name=\"button1id\" class=\"btn btn-success\">Submit</button>\n"; 
          $data['content'] .= "  </div>\n"; //controls
          $data['content'] .= "</div>    \n"; //control-group
          $data['content'] .= "\n"; 
          $data['content'] .= "</fieldset>\n"; 
          $data['content'] .= "</form>\n";

          $data['content'] .= "</div>"; //.row
          $data['content'] .= "</div>"; //#page-wrapper
          $data['content'] .= "</div>"; //#wrapper
          // $data['content'] .= "</div>";
          // $data['content'] .= "</div>";

          $this->load->view('admin/admin_view3', $data);
	}

	
	function search()
    {
    	$this->adminmenu->show_menu();

        $parttype = $this->input->post('parttype');
        $day = $this->input->post('day');
        $time = $this->input->post('time');


        //Students
        if($parttype == 1)
        {
            //get students
            //$u_day = strtotime($day);
            
            $dateID = $this->debate_model->get_dateID($day);
            $timeID = $this->debate_model->get_timeID($time);
            $dateTimeID = $this->debate_model->get_datetimeID($dateID, $timeID);

            $data['students'] = $this->debate_model->get_avail_students($dateTimeID);
            // echo '<pre>';
            // echo var_dump($data['students']);
            // echo '</pre>';
            $data['content'] = " ";
			$data['content'] .= '<div id="page-wrapper">';
			$data['content'] .= '           <div class="row">';
			$data['content'] .= '                <div class="col-lg-12">';
			$data['content'] .= '                    <h1 class="page-header"></h1>';
			$data['content'] .= '                </div>';
            if(!empty($data['students']))
            {

                $data['content'] .= "<div class=\"col-lg-6\">\n"; 
                $data['content'] .= "                    <div class=\"panel panel-default\">\n"; 
                $data['content'] .= "                        <div class=\"panel-heading\">\n"; 
                $data['content'] .= "                            Available Students\n"; 
                $data['content'] .= "                        </div>\n"; 
                $data['content'] .= "                        <!-- /.panel-heading -->\n"; 
                $data['content'] .= "                        <div class=\"panel-body\">\n"; 
                $data['content'] .= "                            <div class=\"table-responsive\">\n"; 
                $data['content'] .= "                                <table class=\"table\">\n"; 
                $data['content'] .= "                                    <thead>\n"; 
                $data['content'] .= "                                        <tr>\n"; 
                $data['content'] .= "                                            <th>#</th>\n"; 
                $data['content'] .= "                                            <th>First Name</th>\n"; 
                $data['content'] .= "                                            <th>Last Name</th>\n"; 
                $data['content'] .= "                                            <th>Major</th>\n";
                $data['content'] .= "                                            <th>Grade</th>\n";
                $data['content'] .= "                                            <th>Email</th>\n"; 
                $data['content'] .= "                                        </tr>\n"; 
                $data['content'] .= "                                    </thead>\n";
                $data['content'] .= "                                    <tbody>\n";

                $count = 1;
                foreach($data['students'] as $s)
                {

                     
                    $data['content'] .= "                                        <tr>\n"; 
                    $data['content'] .= "                                            <td>$count</td>\n"; 
                    $data['content'] .= "                                            <td>$s->fname</td>\n"; 
                    $data['content'] .= "                                            <td>$s->lname</td>\n"; 
                    $data['content'] .= "                                            <td>$s->major</td>\n";
                    $data['content'] .= "                                            <td>$s->class_title</td>\n";
                    $data['content'] .= "                                            <td>$s->email</td>\n"; 
                    $data['content'] .= "                                        </tr>\n"; 
                    
                    $count++;
                }
                    $data['content'] .= "                                    </tbody>\n"; 
                    $data['content'] .= "                                </table>\n"; 
                    $data['content'] .= "                            </div>\n"; 
                    $data['content'] .= "                            <!-- /.table-responsive -->\n"; 
                    $data['content'] .= "                        </div>\n"; 
                    $data['content'] .= "                        <!-- /.panel-body -->\n"; 
                    $data['content'] .= "                    </div>\n"; 
                    $data['content'] .= "                    <!-- /.panel -->\n"; 
                    $data['content'] .= "                </div>\n"; 
                    $data['content'] .= "                <!-- /.col-lg-6 -->\n";
                    $data['content'] .= "<ul class='pager'>";
                    $data['content'] .= "<li class='previous'><a href='http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/admin/schedules'>&larr; Back to search</a></li>";
                    $data['content'] .= "</ul>";


            }//end if
            else
            {
                $data['content'] .= "<div class='alert alert-danger alert-dismissable'>";
                $data['content'] .= "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
                $data['content'] .= "There are no matches for your search.";
                $data['content'] .= "</div>";

                $data['content'] .= "<ul class='pager'>";
                $data['content'] .= "<li class='previous'><a href='http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/admin/schedules'>&larr; Try again</a></li>";
                $data['content'] .= "</ul>";
            }//end else

			$data['content'] .= '</div>';
			$data['content'] .= '            <!-- /.row -->';
			$data['content'] .= '        </div>';
			$data['content'] .= '        <!-- /#page-wrapper -->';
			$data['content'] .= '    </div>';
			$data['content'] .= '    <!-- /#wrapper -->';
        }//end if($parttype == 1)

        //Judges
        if($parttype == 2)
        {
            //get judges
            
			$dateID = $this->debate_model->get_dateID($day);
            $timeID = $this->debate_model->get_timeID($time);
            $dateTimeID = $this->debate_model->get_datetimeID($dateID, $timeID);

            $data['judges'] = $this->debate_model->get_avail_judges($dateTimeID);
            // echo '<pre>';
            // echo var_dump($data['judges']);
            // echo '</pre>';
            $data['content'] = " ";
        	$data['content'] .= '<div id="page-wrapper">';
			$data['content'] .= '           <div class="row">';
			$data['content'] .= '                <div class="col-lg-12">';
			$data['content'] .= '                    <h1 class="page-header"></h1>';
			$data['content'] .= '                </div>';
            if(!empty($data['judges']))
            {


                $data['content'] .= "<div class=\"col-lg-6\">\n"; 
                $data['content'] .= "                    <div class=\"panel panel-default\">\n"; 
                $data['content'] .= "                        <div class=\"panel-heading\">\n"; 
                $data['content'] .= "                            Available Judges\n"; 
                $data['content'] .= "                        </div>\n"; 
                $data['content'] .= "                        <!-- /.panel-heading -->\n"; 
                $data['content'] .= "                        <div class=\"panel-body\">\n"; 
                $data['content'] .= "                            <div class=\"table-responsive\">\n"; 
                $data['content'] .= "                                <table class=\"table\">\n"; 
                $data['content'] .= "                                    <thead>\n"; 
                $data['content'] .= "                                        <tr>\n"; 
                $data['content'] .= "                                            <th>#</th>\n"; 
                $data['content'] .= "                                            <th>First Name</th>\n"; 
                $data['content'] .= "                                            <th>Last Name</th>\n"; 
                $data['content'] .= "                                            <th>Email</th>\n"; 
                $data['content'] .= "                                        </tr>\n"; 
                $data['content'] .= "                                    </thead>\n";
                $data['content'] .= "                                    <tbody>\n";

                $count = 1;
                foreach($data['judges'] as $j)
                {

                     
                    $data['content'] .= "                                        <tr>\n"; 
                    $data['content'] .= "                                            <td>$count</td>\n"; 
                    $data['content'] .= "                                            <td>$j->fname</td>\n"; 
                    $data['content'] .= "                                            <td>$j->lname</td>\n"; 
                    $data['content'] .= "                                            <td>$j->email</td>\n"; 
                    $data['content'] .= "                                        </tr>\n"; 
                    
                    $count++;
                }
                    $data['content'] .= "                                    </tbody>\n"; 
                    $data['content'] .= "                                </table>\n"; 
                    $data['content'] .= "                            </div>\n"; 
                    $data['content'] .= "                            <!-- /.table-responsive -->\n"; 
                    $data['content'] .= "                        </div>\n"; 
                    $data['content'] .= "                        <!-- /.panel-body -->\n"; 
                    $data['content'] .= "                    </div>\n"; 
                    $data['content'] .= "                    <!-- /.panel -->\n"; 
                    $data['content'] .= "                </div>\n"; 
                    $data['content'] .= "                <!-- /.col-lg-6 -->\n";
                    $data['content'] .= "<ul class='pager'>";
                    $data['content'] .= "<li class='previous'><a href='http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/admin/schedules'>&larr; Back to search</a></li>";
                    $data['content'] .= "</ul>";

            }//end if
            else
            {
                $data['content'] .= "<div class='alert alert-danger alert-dismissable'>";
                $data['content'] .= "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
                $data['content'] .= "There are no matches for your search.";
                $data['content'] .= "</div>";

                $data['content'] .= "<ul class='pager'>";
                $data['content'] .= "<li class='previous'><a href='http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/admin/schedules'>&larr; Try again</a></li>";
                $data['content'] .= "</ul>";
            }
			$data['content'] .= '</div>';
			$data['content'] .= '            <!-- /.row -->';
			$data['content'] .= '        </div>';
			$data['content'] .= '        <!-- /#page-wrapper -->';
			$data['content'] .= '    </div>';
			$data['content'] .= '    <!-- /#wrapper -->';
        }//end if judge

        //Student and Judges
        if($parttype == 3)
        {
            //get both students and judges

          //get students
            $u_day = strtotime($day);
            
			$dateID = $this->debate_model->get_dateID($day);
            $timeID = $this->debate_model->get_timeID($time);
            $dateTimeID = $this->debate_model->get_datetimeID($dateID, $timeID);

            $data['students'] = $this->debate_model->get_avail_students($dateTimeID);
            $data['content'] = " ";
			$data['content'] .= '<div id="page-wrapper">';
			$data['content'] .= '           <div class="row">';
			$data['content'] .= '                <div class="col-lg-12">';
			$data['content'] .= '                    <h1 class="page-header"></h1>';
			$data['content'] .= '                </div>';
            if(!empty($data['students']))
            {

                $data['content'] .= "<div class=\"col-lg-6\">\n"; 
                $data['content'] .= "                    <div class=\"panel panel-default\">\n"; 
                $data['content'] .= "                        <div class=\"panel-heading\">\n"; 
                $data['content'] .= "                            Available Students\n"; 
                $data['content'] .= "                        </div>\n"; 
                $data['content'] .= "                        <!-- /.panel-heading -->\n"; 
                $data['content'] .= "                        <div class=\"panel-body\">\n"; 
                $data['content'] .= "                            <div class=\"table-responsive\">\n"; 
                $data['content'] .= "                                <table class=\"table\">\n"; 
                $data['content'] .= "                                    <thead>\n"; 
                $data['content'] .= "                                        <tr>\n"; 
                $data['content'] .= "                                            <th>#</th>\n"; 
                $data['content'] .= "                                            <th>First Name</th>\n"; 
                $data['content'] .= "                                            <th>Last Name</th>\n"; 
                $data['content'] .= "                                            <th>Major</th>\n";
                $data['content'] .= "                                            <th>Grade</th>\n";
                $data['content'] .= "                                            <th>Email</th>\n";
                $data['content'] .= "                                        </tr>\n"; 
                $data['content'] .= "                                    </thead>\n";
                $data['content'] .= "                                    <tbody>\n";

                $count = 1;
                foreach($data['students'] as $s)
                {

                     
                    $data['content'] .= "                                        <tr>\n"; 
                    $data['content'] .= "                                            <td>$count</td>\n"; 
                    $data['content'] .= "                                            <td>$s->fname</td>\n"; 
                    $data['content'] .= "                                            <td>$s->lname</td>\n"; 
                    $data['content'] .= "                                            <td>$s->major</td>\n";
                    $data['content'] .= "                                            <td>$s->class_title</td>\n";
                    $data['content'] .= "                                            <td>$s->email</td>\n";
                    $data['content'] .= "                                        </tr>\n"; 
                    
                    $count++;
                }
                    $data['content'] .= "                                    </tbody>\n"; 
                    $data['content'] .= "                                </table>\n"; 
                    $data['content'] .= "                            </div>\n"; 
                    $data['content'] .= "                            <!-- /.table-responsive -->\n"; 
                    $data['content'] .= "                        </div>\n"; 
                    $data['content'] .= "                        <!-- /.panel-body -->\n"; 
                    $data['content'] .= "                    </div>\n"; 
                    $data['content'] .= "                    <!-- /.panel -->\n"; 
                    $data['content'] .= "                </div>\n"; 
                    $data['content'] .= "                <!-- /.col-lg-6 -->\n";


            }//end if
            elseif(empty($data['students']))
            {
                $data['content'] .= "<div class='alert alert-danger alert-dismissable'>";
                $data['content'] .= "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
                $data['content'] .= "There are no student matches for your search.";
                $data['content'] .= "</div>";

            }







            //get judges

            $data['judges'] = $this->debate_model->get_avail_judges($dateTimeID);
            if(!empty($data['judges']))
            {

                $data['content'] .= "<div class=\"col-lg-6\">\n"; 
                $data['content'] .= "                    <div class=\"panel panel-default\">\n"; 
                $data['content'] .= "                        <div class=\"panel-heading\">\n"; 
                $data['content'] .= "                            Available Judges\n"; 
                $data['content'] .= "                        </div>\n"; 
                $data['content'] .= "                        <!-- /.panel-heading -->\n"; 
                $data['content'] .= "                        <div class=\"panel-body\">\n"; 
                $data['content'] .= "                            <div class=\"table-responsive\">\n"; 
                $data['content'] .= "                                <table class=\"table\">\n"; 
                $data['content'] .= "                                    <thead>\n"; 
                $data['content'] .= "                                        <tr>\n"; 
                $data['content'] .= "                                            <th>#</th>\n"; 
                $data['content'] .= "                                            <th>First Name</th>\n"; 
                $data['content'] .= "                                            <th>Last Name</th>\n"; 
                $data['content'] .= "                                            <th>Email</th>\n"; 
                $data['content'] .= "                                        </tr>\n"; 
                $data['content'] .= "                                    </thead>\n";
                $data['content'] .= "                                    <tbody>\n";

                $count = 1;
                foreach($data['judges'] as $j)
                {

                     
                    $data['content'] .= "                                        <tr>\n"; 
                    $data['content'] .= "                                            <td>$count</td>\n"; 
                    $data['content'] .= "                                            <td>$j->fname</td>\n"; 
                    $data['content'] .= "                                            <td>$j->lname</td>\n"; 
                    $data['content'] .= "                                            <td>$j->email</td>\n"; 
                    $data['content'] .= "                                        </tr>\n"; 
                    
                    $count++;
                }
                    $data['content'] .= "                                    </tbody>\n"; 
                    $data['content'] .= "                                </table>\n"; 
                    $data['content'] .= "                            </div>\n"; 
                    $data['content'] .= "                            <!-- /.table-responsive -->\n"; 
                    $data['content'] .= "                        </div>\n"; 
                    $data['content'] .= "                        <!-- /.panel-body -->\n"; 
                    $data['content'] .= "                    </div>\n"; 
                    $data['content'] .= "                    <!-- /.panel -->\n"; 
                    $data['content'] .= "                </div>\n"; 
                    $data['content'] .= "                <!-- /.col-lg-6 -->\n";
                    $data['content'] .= "<ul class='pager'>";
                    $data['content'] .= "<li class='previous'><a href='http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/admin/schedules'>&larr; Back to search</a></li>";
                    $data['content'] .= "</ul>";

            }//end if
            elseif(empty($data['judges']))
            {

                $data['content'] .= "<div class='alert alert-danger alert-dismissable'>";
                $data['content'] .= "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
                $data['content'] .= "There are no Judge matches for your search.";
                $data['content'] .= "</div>";

                $data['content'] .= "<ul class='pager'>";
                $data['content'] .= "<li class='previous'><a href='http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/admin/schedules'>&larr; Try again</a></li>";
                $data['content'] .= "</ul>";
            }

        }//students and judges

        //$this->load->view('admin/schedules_view', $data);
        $this->load->view('admin/admin_view3', $data);
    }// end search()




    function remove_from_avail()
    {

    	$this->adminmenu->show_menu();

		$data['students'] = $this->debate_model->get_all_students();
		// echo '<pre>';
		// echo var_dump($data['students']);
		// echo '</pre>';


		$data['content'] = '<div id="page-wrapper">';
		$data['content'] .= '           <div class="row">';
		$data['content'] .= '                <div class="col-lg-12">';
		$data['content'] .= '                    <h1 class="page-header"></h1>';
		$data['content'] .= '                </div>';

		$data['content'] .= '		<div class="panel-group" id="accordion">';
		$data['content'] .= '  <div class="panel panel-default">';
		$data['content'] .= '    <div class="panel-heading">';
		$data['content'] .= '      <h4 class="panel-title">';
		$data['content'] .= '        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">';
		$data['content'] .= '          Disqualify Students';
		$data['content'] .= '        </a>';
		$data['content'] .= '      </h4>';
		$data['content'] .= '    </div>';
		$data['content'] .= '    <div id="collapseOne" class="panel-collapse collapse">';
		$data['content'] .= '      <div class="panel-body">';

		$data['content'] .= '<div class="row">'; 
		$data['content'] .= "                <div class='col-lg-12'>"; 
		$data['content'] .= "                    <div class='panel panel-default'>"; 
		$data['content'] .= "                        <div class='panel-heading'>"; 
		$data['content'] .= "                            Select one or more students below so their schedules do not appear when you're viewing schedules. Students in <font color='red'><b>red</b></font> have already been eliminated and thier schedules will not appear."; 
		$data['content'] .= "                        </div>"; 
		$data['content'] .= "                        <div class='panel-body'>"; 
		$data['content'] .= "                            <div class='row'>"; 
		$data['content'] .= "                                <div class='col-lg-6'>";

		$data['content'] .= "<form role='form' action='./drop_from_avail' method='post'>";
		$data['content'] .= "<div class='form-group'>";
		//$data['content'] .= "<label>Participant Type</label>";

		foreach($data['students'] as $s)
		{   
			$username = $s->username;

			$fname = $s->fname;
			$lname = $s->lname;

			$fullname = $fname.' '.$lname;

			$data['qualified'] = $this->debate_model->is_stu_avail($username);
			if (!empty($data['qualified']))
			{
				$data['content'] .= "<div class='checkbox'>";
				$data['content'] .= "<label>";
				$data['content'] .= "<input type='checkbox' name='students[names][]' value='$username'><font color='green'>$fullname</font>";
				$data['content'] .= "</label>";
				$data['content'] .= "</div>";
			}

			else
			{
				$data['content'] .= "<div class='checkbox'>";
				$data['content'] .= "<label>";
				$data['content'] .= "<input type='checkbox' name='students[names][]' value='$username' disabled><font color='red'>$fullname</font>";
				$data['content'] .= "</label>";
				$data['content'] .= "</div>";
			}

			//$data['avail'] = $this->debate_model->is_stu_avail($username);
			// if(empty($data['avail']))
			// {
			// 	$data['content'] .= "I'm disqualified!";
			// }


		}


		$data['content'] .= "</div><!-- form-group -->";
		$data['content'] .= "<button type='submit' class='btn btn-default'>Submit</button>";
		$data['content'] .= "<button type='reset' class='btn btn-default'>Reset</button>";
		$data['content'] .= "</form>";
		$data['content'] .= "</div>"; //col-lg-6
		$data['content'] .= "                                <!-- /.col-lg-6 (nested) -->"; 
		$data['content'] .= "                            </div>"; //row
		$data['content'] .= "                            <!-- /.row (nested) -->"; 
		$data['content'] .= "                        </div>"; 
		$data['content'] .= "                        <!-- /.panel-body -->"; 
		$data['content'] .= "                    </div>"; //panel-heading 
		$data['content'] .= "                </div>"; //panel panel-default
		$data['content'] .= "            </div>";//col-lg-12
		$data['content'] .= "		</div>"; //row

		$data['content'] .= "		</div>"; //next three divs are for the collapse
		$data['content'] .= "    </div>";
		$data['content'] .= "  </div>";
		
		//---------------------------------------------------------------------------------------------------------
		//---------------------------------------------------------------------------------------------------------
		//---------------------------------------------------------------------------------------------------------


		$data['content'] .= '<div class="panel panel-default">';
		$data['content'] .= '    <div class="panel-heading">';
		$data['content'] .= '      <h4 class="panel-title">';
		$data['content'] .= '        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">';
		$data['content'] .= '          Add Student Back In';
		$data['content'] .= '        </a>';
		$data['content'] .= '      </h4>';
		$data['content'] .= '    </div>';
		$data['content'] .= '    <div id="collapseTwo" class="panel-collapse collapse">';
		$data['content'] .= '      <div class="panel-body">';

		$data['content'] .= '<div class="row">'; 
		$data['content'] .= "                <div class='col-lg-12'>"; 
		$data['content'] .= "                    <div class='panel panel-default'>"; 
		$data['content'] .= "                        <div class='panel-heading'>"; 
		$data['content'] .= "                            Select one or more students below to label them as qualified. Students in <font color='3E89D4'><b>blue</b></font> can be added back in to the debate."; 
		$data['content'] .= "                        </div>"; 
		$data['content'] .= "                        <div class='panel-body'>"; 
		$data['content'] .= "                            <div class='row'>"; 
		$data['content'] .= "                                <div class='col-lg-6'>";

		$data['content'] .= "<form role='form' action='./add_to_avail' method='post'>";
		$data['content'] .= "<div class='form-group'>";
		//$data['content'] .= "<label>Participant Type</label>";

		foreach($data['students'] as $s)
		{   
			$username = $s->username;

			$fname = $s->fname;
			$lname = $s->lname;

			$fullname = $fname.' '.$lname;

			$data['qualified'] = $this->debate_model->is_stu_out($username);
			if (!empty($data['qualified']))
			{
				$data['content'] .= "<div class='checkbox'>";
				$data['content'] .= "<label>";
				$data['content'] .= "<input type='checkbox' name='students[names][]' value='$username'><font color='3E89D4'><b>$fullname</b></font></b>";
				$data['content'] .= "</label>";
				$data['content'] .= "</div>";
			}

			else
			{
				$data['content'] .= "<div class='checkbox'>";
				$data['content'] .= "<label>";
				$data['content'] .= "<input type='checkbox' name='students[names][]' value='$username' disabled>$fullname";
				$data['content'] .= "</label>";
				$data['content'] .= "</div>";
			}

			//$data['avail'] = $this->debate_model->is_stu_avail($username);
			// if(empty($data['avail']))
			// {
			// 	$data['content'] .= "I'm disqualified!";
			// }


		}


		$data['content'] .= "</div><!-- form-group -->";
		$data['content'] .= "<button type='submit' class='btn btn-default'>Submit</button>";
		$data['content'] .= "<button type='reset' class='btn btn-default'>Reset</button>";
		$data['content'] .= "</form>";
		$data['content'] .= "</div>"; //col-lg-6
		$data['content'] .= "                                <!-- /.col-lg-6 (nested) -->"; 
		$data['content'] .= "                            </div>"; //row
		$data['content'] .= "                            <!-- /.row (nested) -->"; 
		$data['content'] .= "                        </div>"; 
		$data['content'] .= "                        <!-- /.panel-body -->"; 
		$data['content'] .= "                    </div>"; //panel-heading 
		$data['content'] .= "                </div>"; //panel panel-default
		$data['content'] .= "            </div>";//col-lg-12
		$data['content'] .= "		</div>"; //row

		$data['content'] .= "		</div>"; //next three divs are for the collapse
		$data['content'] .= "    </div>";
		$data['content'] .= "  </div>";



		$data['content'] .= "	</div>";//row
		$data['content'] .= "	</div>";//page-wrapper


		// foreach($data['students'] as $s)
		// {
		//   $data['content'] .= $s->username.'<br />';
		// }
		//$this->load->view('admin/schedules_view',$data);
		$this->load->view('admin/admin_view3',$data);
    }//end remove_from_avail()


    //sends an array of students to a function in the model that sets the student's qualified status to 0 (meaning he/she is disqualified)
    function drop_from_avail()
    {
      $students = (is_array($_POST['students']['names'])) ? $_POST['students']['names'] : array();
      foreach($students as $s)
      {
        //echo 'hi<br />';
        $this->debate_model->drop_from_savail($s);
      }
      redirect('admin/remove_from_avail','refresh');
    }

    //sends an array of students to a function in the model that sets the student's qualified status to 1 (meaning he/she is qualified)
    function add_to_avail()
    {
    	$students = (is_array($_POST['students']['names'])) ? $_POST['students']['names'] : array();
		foreach($students as $s)
		{
			//echo 'hi<br />';
			$this->debate_model->make_student_avail($s);
		}
		redirect('admin/remove_from_avail','refresh');	
    }




    function restart()
    {

    	$this->adminmenu->show_menu();


        $data['content'] = '<div id="page-wrapper">';
		$data['content'] .= '            <div class="row">';
		$data['content'] .= '                <div class="col-lg-12">';
		$data['content'] .= '                    <h1 class="page-header">Restart</h1>';
		$data['content'] .= '                </div>';
		$data['content'] .= '                <!-- /.col-lg-12 -->';
		$data['content'] .= '            </div>';
		$data['content'] .= '            <!-- /.row -->';

		$data['content'] .= '<div id="page=wrapper">';
		$data['content'] .= '<div class="row">'; 
		$data['content'] .= "                <div class='col-lg-12'>";

		$data['content'] .= "                    <div class='panel panel-default'>"; 
		$data['content'] .= "                        <div class='panel-body'>"; 
		$data['content'] .= "                            <div class='row'>"; 
		$data['content'] .= "                                <div class='col-lg-6'>";

		$data['content'] .= "Restarting deletes the old debate calendar. It also deletes eveyone's submitted times. <b>This cannot be undone</b>. If you wish to continue, check 'Continue' then click 'Restart'.";

		$data['content'] .='        <form id="registrationForm" role="form" action="./complete_restart" method="post">';

		$data['content'] .= '                            <div class="form-group">';
		$data['content'] .='                                            <div class="checkbox">';
		$data['content'] .='                                                <label>';
		$data['content'] .='                                                    <input type="checkbox" name="continueRestart" value="1">Continue';
		$data['content'] .='                                                </label>';
		$data['content'] .='                                            </div>';
		$data['content'] .= '                            </div>';

		$data['content'] .= '							<button type="submit" class="btn btn-danger">Restart</button>';
		$data['content'] .= '                        </form>';

		$data['content'] .= '								</div>';//col-lg-6
		$data['content'] .= '							</div>';//row
		$data['content'] .= '						</div>';//panel-body
		$data['content'] .= '					</div>';//panel panel-default

		$data['content'] .= "</div>";//col-lg-12
		$data['content'] .= "</div>";//row
		$data['content'] .= "</div>";//page-wrapper

		$data['content'] .= '        </div>';//page-wrapper
		$data['content'] .= '        <!-- /#page-wrapper -->';


		$this->load->view('admin/form_tester', $data);
    }

    function complete_restart()
    {
    	// echo '<pre>';
    	// echo var_dump($_POST);
    	// echo '</pre>';

    	$continueRestart = $this->input->post('continueRestart');
    	if($continueRestart == 1)
    	{
    		$this->debate_model->restart();	
    	}

    	redirect('/admin', 'refresh');
    }


	function is_logged_in()
    {
        $is_logged_in = $this->session->userdata('is_logged_in');
        $acc_level = $this->session->userdata('acc_level');
        
        if(!isset($is_logged_in) || ($is_logged_in != true) || ($acc_level != 1) )
        {
            echo 'You don\'t have permission to access this page. <a href="http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/login2">Login</a>';
            die();
        }
    }
	
	
	
	





}
