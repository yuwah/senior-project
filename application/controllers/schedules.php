<?php
class Schedules extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('debate_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('url');
    $this->is_logged_in();
	}

    function is_logged_in()
    {
        $is_logged_in = $this->session->userdata('is_logged_in');
        
        if(!isset($is_logged_in) || $is_logged_in != true)
        {
            echo 'You don\'t have permission to access this page. <a href="../login2">Login</a>';
            die();
        }
    }
	
	public function index()
	{
        
        $data['content'] = " ";
        $data['content'] .= "<form role='form' action='./schedules/search' method='post'>\n";
          $data['content'] .= "<fieldset>\n"; 
          $data['content'] .= "\n"; 
          $data['content'] .= "<!-- Form Name -->\n"; 
          $data['content'] .= "<legend>Select which participants you would like to view below:</legend>\n"; 
          $data['content'] .= "\n"; 
          $data['content'] .= "<!-- Select Basic -->\n"; 
          $data['content'] .= "<div class=\"control-group\">\n"; 
          $data['content'] .= "  <label class=\"control-label\" for=\"parttype\">Select all </label>\n"; 
          $data['content'] .= "\n"; 
          $data['content'] .= "    <select id=\"parttype\" name=\"parttype\" class=\"input-large\">\n"; 
          $data['content'] .= "      <option value = '1'>Students</option>\n"; 
          $data['content'] .= "      <option value = '2'>Judges</option>\n";
          $data['content'] .= "      <option value = '3'>Students and Judges</option>\n";
          $data['content'] .= "    </select>\n"; 
          $data['content'] .= "  \n"; 
          $data['content'] .= "  <label class=\"control-label\" for=\"day\"> available on </label>\n"; 
          $data['content'] .= "    <select id=\"day\" name=\"day\" class=\"input-large\">\n";


          $data['days'] = $this->debate_model->get_all_deb_days();
          foreach($data['days'] as $d)
          {
                $day_unix = $d->date;
                $theday = date("D F j, Y", $day_unix);
                $data['content'] .= "<option>$theday</option>\n";
          }

          $data['content'] .= "    </select>\n"; 
          $data['content'] .= "    \n"; 
          $data['content'] .= "    <label class=\"control-label\" for=\"time\"> at </label>\n"; 
          $data['content'] .= "    <select id=\"time\" name=\"time\" class=\"input-large\">\n"; 
          $data['content'] .= "      <option value ='1'>8:30 AM</option>\n"; 
          $data['content'] .= "      <option value = '2'>9:00 AM</option>\n"; 
          $data['content'] .= "      <option value = '3'>9:30 AM</option>\n"; 
          $data['content'] .= "      <option value = '4'>10:00 AM</option>\n"; 
          $data['content'] .= "      <option value = '5'>10:30 AM</option>\n"; 
          $data['content'] .= "      <option value = '6'>11:00 AM</option>\n"; 
          $data['content'] .= "      <option value = '7'>11:30 AM</option>\n"; 
          $data['content'] .= "      <option value = '8'>12:00 PM</option>\n"; 
          $data['content'] .= "      <option value = '9'>12:30 PM</option>\n"; 
          $data['content'] .= "      <option value = '10'>1:00 PM</option>\n"; 
          $data['content'] .= "      <option value = '11'>1:30 PM</option>\n"; 
          $data['content'] .= "      <option value = '12'>2:00 PM</option>\n"; 
          $data['content'] .= "      <option value = '13'>2:30 PM</option>\n"; 
          $data['content'] .= "      <option value = '14'>3:00 PM</option>\n"; 
          $data['content'] .= "      <option value = '15'>3:30 PM</option>\n"; 
          $data['content'] .= "      <option value = '16'>4:00 PM</option>\n";
          $data['content'] .= "      <option value = '17'>4:30 PM</option>\n";
          $data['content'] .= "      <option value = '18'>5:00 PM</option>\n";
          $data['content'] .= "      <option value = '19'>5:30 PM</option>\n";
          $data['content'] .= "      <option value = '20'>6:00 PM</option>\n";
          $data['content'] .= "      <option value = '21'>6:30 PM</option>\n";
          $data['content'] .= "      <option value = '22'>7:00 PM</option>\n";
          $data['content'] .= "      <option value = '23'>7:30 PM</option>\n";
          $data['content'] .= "      <option value = '24'>8:00 PM</option>\n";
          $data['content'] .= "      <option value = '25'>8:30 PM</option>\n";
          $data['content'] .= "    </select>\n"; 
          $data['content'] .= "  \n"; 
          $data['content'] .= "      \n"; 
          $data['content'] .= "  </div>\n"; 
          $data['content'] .= "</div>\n"; 
          $data['content'] .= "\n"; 
          $data['content'] .= "    \n"; 
          $data['content'] .= "<!-- Button (Double) -->\n"; 
          $data['content'] .= "<div class=\"control-group\">\n"; 
          $data['content'] .= "  <label class=\"control-label\" for=\"button1id\"></label>\n"; 
          $data['content'] .= "  <div class=\"controls\">\n"; 
          $data['content'] .= "    <button id=\"button1id\" name=\"button1id\" class=\"btn btn-success\">Submit</button>\n"; 
          $data['content'] .= "  </div>\n"; 
          $data['content'] .= "</div>    \n"; 
          $data['content'] .= "\n"; 
          $data['content'] .= "</fieldset>\n"; 
          $data['content'] .= "</form>\n";


        // $students = $this->debate_model->get_all_students();
        // //$judges = $this->debate_model->get_all_judges();
        
        // $data['content'] = '<div>';
        // $data['content'] .= "<h4 class='page-header'>Students</h4>";
        // foreach ($students as $s)
        // {
        //     $data['content'] .= '<h4>'.$s->fname.' '.$s->lname.'</h4>';
        //     $data['content'] .= '<blockquote>Major: '.$s->major.'<br>';
        //     $data['content'] .= 'Email: '.$s->email.'</blockquote><br>';
        // }
        // $data['content'] .= '</div>';
        
        // $data['content'] .= '<div>';
        // $data['content'] .= "<h4 class='page-header'>Judges</h4>";
        // $data['judges'] = $this->debate_model->get_all_judges();
        // foreach($data['judges'] as $j)
        // {
        //     $data['content'] .= '<h4>'.$j->fname.' '.$j->lname.'</h4>';
        //     $data['content'] .= '<blockquote>Department: '.$j->title.'<br>';
        //     $data['content'] .= 'Email: '.$j->email.'</blockquote><br>';
        // }
        // $data['content'] .= '</div>';
        $this->load->view('admin/schedules_view', $data);
	}
	

    function search()
    {
        $parttype = $this->input->post('parttype');
        $day = $this->input->post('day');
        $time = $this->input->post('time');


        //Students
        if($parttype == 1)
        {
            //get students
            $u_day = strtotime($day);
            
            $dateID = $this->debate_model->get_dateID($u_day);
            $timeID = $this->debate_model->get_timeID();
            $dateTimeID = $this->debate_model->get_datetimeID($dateID, $timeID);

            $data['students'] = $this->debate_model->get_avail_students($dateTimeID);
            $data['content'] = " ";
            if(!empty($data['students']))
            {
                $data['content'] .= "<div class=\"col-lg-6\">\n"; 
                $data['content'] .= "                    <div class=\"panel panel-default\">\n"; 
                $data['content'] .= "                        <div class=\"panel-heading\">\n"; 
                $data['content'] .= "                            Available Students\n"; 
                $data['content'] .= "                        </div>\n"; 
                $data['content'] .= "                        <!-- /.panel-heading -->\n"; 
                $data['content'] .= "                        <div class=\"panel-body\">\n"; 
                $data['content'] .= "                            <div class=\"table-responsive\">\n"; 
                $data['content'] .= "                                <table class=\"table\">\n"; 
                $data['content'] .= "                                    <thead>\n"; 
                $data['content'] .= "                                        <tr>\n"; 
                $data['content'] .= "                                            <th>#</th>\n"; 
                $data['content'] .= "                                            <th>First Name</th>\n"; 
                $data['content'] .= "                                            <th>Last Name</th>\n"; 
                $data['content'] .= "                                            <th>Username</th>\n"; 
                $data['content'] .= "                                        </tr>\n"; 
                $data['content'] .= "                                    </thead>\n";
                $data['content'] .= "                                    <tbody>\n";

                $count = 1;
                foreach($data['students'] as $s)
                {

                     
                    $data['content'] .= "                                        <tr>\n"; 
                    $data['content'] .= "                                            <td>$count</td>\n"; 
                    $data['content'] .= "                                            <td>$s->fname</td>\n"; 
                    $data['content'] .= "                                            <td>$s->lname</td>\n"; 
                    $data['content'] .= "                                            <td>$s->username</td>\n"; 
                    $data['content'] .= "                                        </tr>\n"; 
                    
                    $count++;
                }
                    $data['content'] .= "                                    </tbody>\n"; 
                    $data['content'] .= "                                </table>\n"; 
                    $data['content'] .= "                            </div>\n"; 
                    $data['content'] .= "                            <!-- /.table-responsive -->\n"; 
                    $data['content'] .= "                        </div>\n"; 
                    $data['content'] .= "                        <!-- /.panel-body -->\n"; 
                    $data['content'] .= "                    </div>\n"; 
                    $data['content'] .= "                    <!-- /.panel -->\n"; 
                    $data['content'] .= "                </div>\n"; 
                    $data['content'] .= "                <!-- /.col-lg-6 -->\n";
                    $data['content'] .= "<ul class='pager'>";
                    $data['content'] .= "<li class='previous'><a href='http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/schedules'>&larr; Back to search</a></li>";
                    $data['content'] .= "</ul>";

            }//end if
            else
            {
                $data['content'] = "<div class='alert alert-danger alert-dismissable'>";
                $data['content'] .= "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
                $data['content'] .= "There are no matches for your search.";
                $data['content'] .= "</div>";

                $data['content'] .= "<ul class='pager'>";
                $data['content'] .= "<li class='previous'><a href='http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/schedules'>&larr; Try again</a></li>";
                $data['content'] .= "</ul>";
            }
        }

        //Judges
        if($parttype == 2)
        {
            //get judges
            $u_day = strtotime($day);
            
            $dateID = $this->debate_model->get_dateID($u_day);
            $timeID = $this->debate_model->get_timeID();
            $dateTimeID = $this->debate_model->get_datetimeID($dateID, $timeID);

            $data['judges'] = $this->debate_model->get_avail_judges($dateTimeID);
            $data['content'] = " ";
            if(!empty($data['judges']))
            {
                $data['content'] .= "<div class=\"col-lg-6\">\n"; 
                $data['content'] .= "                    <div class=\"panel panel-default\">\n"; 
                $data['content'] .= "                        <div class=\"panel-heading\">\n"; 
                $data['content'] .= "                            Available Judges\n"; 
                $data['content'] .= "                        </div>\n"; 
                $data['content'] .= "                        <!-- /.panel-heading -->\n"; 
                $data['content'] .= "                        <div class=\"panel-body\">\n"; 
                $data['content'] .= "                            <div class=\"table-responsive\">\n"; 
                $data['content'] .= "                                <table class=\"table\">\n"; 
                $data['content'] .= "                                    <thead>\n"; 
                $data['content'] .= "                                        <tr>\n"; 
                $data['content'] .= "                                            <th>#</th>\n"; 
                $data['content'] .= "                                            <th>First Name</th>\n"; 
                $data['content'] .= "                                            <th>Last Name</th>\n"; 
                $data['content'] .= "                                            <th>Username</th>\n"; 
                $data['content'] .= "                                        </tr>\n"; 
                $data['content'] .= "                                    </thead>\n";
                $data['content'] .= "                                    <tbody>\n";

                $count = 1;
                foreach($data['judges'] as $j)
                {

                     
                    $data['content'] .= "                                        <tr>\n"; 
                    $data['content'] .= "                                            <td>$count</td>\n"; 
                    $data['content'] .= "                                            <td>$j->fname</td>\n"; 
                    $data['content'] .= "                                            <td>$j->lname</td>\n"; 
                    $data['content'] .= "                                            <td>$j->username</td>\n"; 
                    $data['content'] .= "                                        </tr>\n"; 
                    
                    $count++;
                }
                    $data['content'] .= "                                    </tbody>\n"; 
                    $data['content'] .= "                                </table>\n"; 
                    $data['content'] .= "                            </div>\n"; 
                    $data['content'] .= "                            <!-- /.table-responsive -->\n"; 
                    $data['content'] .= "                        </div>\n"; 
                    $data['content'] .= "                        <!-- /.panel-body -->\n"; 
                    $data['content'] .= "                    </div>\n"; 
                    $data['content'] .= "                    <!-- /.panel -->\n"; 
                    $data['content'] .= "                </div>\n"; 
                    $data['content'] .= "                <!-- /.col-lg-6 -->\n";
                    $data['content'] .= "<ul class='pager'>";
                    $data['content'] .= "<li class='previous'><a href='http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/schedules'>&larr; Back to search</a></li>";
                    $data['content'] .= "</ul>";

            }//end if
            else
            {
                $data['content'] = "<div class='alert alert-danger alert-dismissable'>";
                $data['content'] .= "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
                $data['content'] .= "There are no matches for your search.";
                $data['content'] .= "</div>";

                $data['content'] .= "<ul class='pager'>";
                $data['content'] .= "<li class='previous'><a href='http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/schedules'>&larr; Try again</a></li>";
                $data['content'] .= "</ul>";
            }
        }//end if judge

        //Student and Judges
        if($parttype == 3)
        {
            //get both students and judges

          //get students
            $u_day = strtotime($day);
            
            $dateID = $this->debate_model->get_dateID($u_day);
            $timeID = $this->debate_model->get_timeID();
            $dateTimeID = $this->debate_model->get_datetimeID($dateID, $timeID);

            $data['students'] = $this->debate_model->get_avail_students($dateTimeID);
            $data['content'] = " ";
            if(!empty($data['students']))
            {
                $data['content'] .= "<div class=\"col-lg-6\">\n"; 
                $data['content'] .= "                    <div class=\"panel panel-default\">\n"; 
                $data['content'] .= "                        <div class=\"panel-heading\">\n"; 
                $data['content'] .= "                            Available Students\n"; 
                $data['content'] .= "                        </div>\n"; 
                $data['content'] .= "                        <!-- /.panel-heading -->\n"; 
                $data['content'] .= "                        <div class=\"panel-body\">\n"; 
                $data['content'] .= "                            <div class=\"table-responsive\">\n"; 
                $data['content'] .= "                                <table class=\"table\">\n"; 
                $data['content'] .= "                                    <thead>\n"; 
                $data['content'] .= "                                        <tr>\n"; 
                $data['content'] .= "                                            <th>#</th>\n"; 
                $data['content'] .= "                                            <th>First Name</th>\n"; 
                $data['content'] .= "                                            <th>Last Name</th>\n"; 
                $data['content'] .= "                                            <th>Username</th>\n"; 
                $data['content'] .= "                                        </tr>\n"; 
                $data['content'] .= "                                    </thead>\n";
                $data['content'] .= "                                    <tbody>\n";

                $count = 1;
                foreach($data['students'] as $s)
                {

                     
                    $data['content'] .= "                                        <tr>\n"; 
                    $data['content'] .= "                                            <td>$count</td>\n"; 
                    $data['content'] .= "                                            <td>$s->fname</td>\n"; 
                    $data['content'] .= "                                            <td>$s->lname</td>\n"; 
                    $data['content'] .= "                                            <td>$s->username</td>\n"; 
                    $data['content'] .= "                                        </tr>\n"; 
                    
                    $count++;
                }
                    $data['content'] .= "                                    </tbody>\n"; 
                    $data['content'] .= "                                </table>\n"; 
                    $data['content'] .= "                            </div>\n"; 
                    $data['content'] .= "                            <!-- /.table-responsive -->\n"; 
                    $data['content'] .= "                        </div>\n"; 
                    $data['content'] .= "                        <!-- /.panel-body -->\n"; 
                    $data['content'] .= "                    </div>\n"; 
                    $data['content'] .= "                    <!-- /.panel -->\n"; 
                    $data['content'] .= "                </div>\n"; 
                    $data['content'] .= "                <!-- /.col-lg-6 -->\n";

            }//end if
            elseif(empty($data['students']))
            {
                $data['content'] = "<div class='alert alert-danger alert-dismissable'>";
                $data['content'] .= "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
                $data['content'] .= "There are no student matches for your search.";
                $data['content'] .= "</div>";

            }







            //get judges

            $data['judges'] = $this->debate_model->get_avail_judges($dateTimeID);
            if(!empty($data['judges']))
            {
                $data['content'] .= "<div class=\"col-lg-6\">\n"; 
                $data['content'] .= "                    <div class=\"panel panel-default\">\n"; 
                $data['content'] .= "                        <div class=\"panel-heading\">\n"; 
                $data['content'] .= "                            Available Judges\n"; 
                $data['content'] .= "                        </div>\n"; 
                $data['content'] .= "                        <!-- /.panel-heading -->\n"; 
                $data['content'] .= "                        <div class=\"panel-body\">\n"; 
                $data['content'] .= "                            <div class=\"table-responsive\">\n"; 
                $data['content'] .= "                                <table class=\"table\">\n"; 
                $data['content'] .= "                                    <thead>\n"; 
                $data['content'] .= "                                        <tr>\n"; 
                $data['content'] .= "                                            <th>#</th>\n"; 
                $data['content'] .= "                                            <th>First Name</th>\n"; 
                $data['content'] .= "                                            <th>Last Name</th>\n"; 
                $data['content'] .= "                                            <th>Username</th>\n"; 
                $data['content'] .= "                                        </tr>\n"; 
                $data['content'] .= "                                    </thead>\n";
                $data['content'] .= "                                    <tbody>\n";

                $count = 1;
                foreach($data['judges'] as $j)
                {

                     
                    $data['content'] .= "                                        <tr>\n"; 
                    $data['content'] .= "                                            <td>$count</td>\n"; 
                    $data['content'] .= "                                            <td>$j->fname</td>\n"; 
                    $data['content'] .= "                                            <td>$j->lname</td>\n"; 
                    $data['content'] .= "                                            <td>$j->username</td>\n"; 
                    $data['content'] .= "                                        </tr>\n"; 
                    
                    $count++;
                }
                    $data['content'] .= "                                    </tbody>\n"; 
                    $data['content'] .= "                                </table>\n"; 
                    $data['content'] .= "                            </div>\n"; 
                    $data['content'] .= "                            <!-- /.table-responsive -->\n"; 
                    $data['content'] .= "                        </div>\n"; 
                    $data['content'] .= "                        <!-- /.panel-body -->\n"; 
                    $data['content'] .= "                    </div>\n"; 
                    $data['content'] .= "                    <!-- /.panel -->\n"; 
                    $data['content'] .= "                </div>\n"; 
                    $data['content'] .= "                <!-- /.col-lg-6 -->\n";
                    $data['content'] .= "<ul class='pager'>";
                    $data['content'] .= "<li class='previous'><a href='http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/schedules'>&larr; Back to search</a></li>";
                    $data['content'] .= "</ul>";

            }//end if
            elseif(empty($data['judges']))
            {
                $data['content'] = "<div class='alert alert-danger alert-dismissable'>";
                $data['content'] .= "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
                $data['content'] .= "There are no Judge matches for your search.";
                $data['content'] .= "</div>";

                $data['content'] .= "<ul class='pager'>";
                $data['content'] .= "<li class='previous'><a href='http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/schedules'>&larr; Try again</a></li>";
                $data['content'] .= "</ul>";
            }




        }//students and judges

        $this->load->view('admin/schedules_view', $data);
    }// end search()

    function remove_from_avail()
    {
      $data['students'] = $this->debate_model->get_all_students();
      $data['content'] = 'hello!';



      $data['content'] = '<div class="row">'; 
      $data['content'] .= "                <div class='col-lg-12'>"; 
      $data['content'] .= "                    <div class='panel panel-default'>"; 
      $data['content'] .= "                        <div class='panel-heading'>"; 
      $data['content'] .= "                            Select eliminated students below:"; 
      $data['content'] .= "                        </div>"; 
      $data['content'] .= "                        <div class='panel-body'>"; 
      $data['content'] .= "                            <div class='row'>"; 
      $data['content'] .= "                                <div class='col-lg-6'>";

      $data['content'] .= "<form role='form' action='./drop_from_avail' method='post'>";
      $data['content'] .= "<div class='form-group'>";
      //$data['content'] .= "<label>Participant Type</label>";

      foreach($data['students'] as $s)
      {   

        $username = $s->username;

        $fname = $s->fname;
        $lname = $s->lname;

        $fullname = $fname.' '.$lname;

        $data['content'] .= "<div class='checkbox'>";
        $data['content'] .= "<label>";
        $data['content'] .= "<input type='checkbox' name='students[names][]' value='$username'>$fullname";
        $data['content'] .= "</label>";
        $data['content'] .= "</div>";

      }


      $data['content'] .= "</div><!-- form-group -->";
      $data['content'] .= "<button type='submit' class='btn btn-default'>Submit</button>";
      $data['content'] .= "<button type='reset' class='btn btn-default'>Reset</button>";
      $data['content'] .= "</form>";
      $data['content'] .= "</div>"; 
      $data['content'] .= "                                <!-- /.col-lg-6 (nested) -->"; 
      $data['content'] .= "                            </div>"; 
      $data['content'] .= "                            <!-- /.row (nested) -->"; 
      $data['content'] .= "                        </div>"; 
      $data['content'] .= "                        <!-- /.panel-body -->"; 
      $data['content'] .= "                    </div>"; 
      $data['content'] .= "                    <!-- /.panel -->"; 
      $data['content'] .= "                </div>"; 
      $data['content'] .= "                <!-- /.col-lg-12 -->"; 
      $data['content'] .= "            </div>";


        // foreach($data['students'] as $s)
        // {
        //   $data['content'] .= $s->username.'<br />';
        // }
        $this->load->view('admin/schedules_view',$data);
    }//end remove_from_avail()


    function drop_from_avail()
    {
      $students = (is_array($_POST['students']['names'])) ? $_POST['students']['names'] : array();
      foreach($students as $s)
      {
        //echo 'hi<br />';
        $this->debate_model->drop_from_savail($s);
      }
      redirect('/schedules/drop_completed','refresh');
    }

    function drop_completed()
    {
        $data['content'] = "<div class='alert alert-success alert-dismissable'>";
        $data['content'] .= "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
        $data['content'] .= "The list of available students has now been updated";
        $data['content'] .= "</div>";

        $this->load->view('admin/schedules_view', $data);
    }
	
	
	
	
	





}
