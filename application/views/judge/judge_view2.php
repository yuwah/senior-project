<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Judge Home</title>

    <!-- Core CSS - Include with every page -->
    <link href="<?php echo base_url('/assets/sb-admin-v2/css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('/assets/sb-admin-v2/font-awesome/css/font-awesome.css');?>" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="<?php echo base_url('/assets/sb-admin-v2/css/plugins/morris/morris-0.4.3.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('/assets/sb-admin-v2/css/plugins/timeline/timeline.css');?>" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="<?php echo base_url('/assets/sb-admin-v2/css/sb-admin.css');?>" rel="stylesheet">

</head>

<body>

    <?php echo $judgenav; ?>
