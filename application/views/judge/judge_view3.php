<?php echo $content; ?>

    <!-- Core Scripts - Include with every page -->
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/jquery-1.10.2.js');?>"></script>
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>

    <!-- Page-Level Plugin Scripts - Dashboard -->
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/plugins/morris/raphael-2.1.0.min.js');?>"></script>
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/plugins/morris/morris.js');?>"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/sb-admin.js');?>"></script>

    <!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/demo/dashboard-demo.js');?>"></script>

</body>

</html>