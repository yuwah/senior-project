<html>
<head>
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url('/assets/sb-admin-v2/css/bootstrap.min.css');?>" rel="stylesheet">

    <!-- Include FontAwesome CSS if you want to use feedback icons provided by FontAwesome -->
    <link href="<?php echo base_url('/assets/sb-admin-v2/font-awesome/css/font-awesome.css');?>" rel="stylesheet">

    <!-- BootstrapValidator CSS -->
    <link rel="stylesheet" href="<?php echo base_url('/assets/bootstrapvalidator-dist-0/dist/css/bootstrapValidator.min.css');?>"/>

    <!-- jQuery and Bootstrap JS -->
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/jquery-1.10.2.js');?>"></script>
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/bootstrap.min.js');?>"></script>

    <!-- BootstrapValidator JS -->
    <script type="text/javascript" src="<?php echo base_url('/assets/bootstrapvalidator-dist-0/dist/js/bootstrapValidator.min.js');?>"></script>
</head>
<body>

                <?php echo $content; ?>

<script>
$(document).ready(function() {
    $('#editAccount').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        live: 'disabled',
        fields: {
            curr_pw: {
                validators: {
                    live: 'enabled',
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    different: {
                        field: 'username',
                        message: 'The password cannot be the same as username'
                    },
                    stringLength: {
                        min: 6,
                        message: 'The password must have at least 6 characters'
                    }
                }
            },
            new_pw: {
                validators: {
                    live: 'enabled',
                    identical: {
                        field: 'confirm',
                        message: 'The password and its confirm are not the same'
                    },
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    different: {
                        field: 'username',
                        message: 'The password cannot be the same as username'
                    },
                    stringLength: {
                        min: 6,
                        message: 'The password must have at least 6 characters'
                    }
                }
            },
            confirm: {
                validators: {
                    identical: {
                        field: 'new_pw',
                        message: 'The password and its confirm are not the same'
                    },
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    different: {
                        field: 'username',
                        message: 'The password cannot be the same as username'
                    },
                    stringLength: {
                        min: 6,
                        message: 'The password must have at least 6 characters'
                    }
                }
            }
        }
    });
});
</script>
</body>
</html>