<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Judge Home</title>

    <!-- Core CSS - Include with every page -->
    <link href="http://www.cs.stedwards.edu/~yuwah/SeniorProject/assets/sb-admin-v2/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://www.cs.stedwards.edu/~yuwah/SeniorProject/assets/sb-admin-v2/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="http://www.cs.stedwards.edu/~yuwah/SeniorProject/assets/sb-admin-v2/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="http://www.cs.stedwards.edu/~yuwah/SeniorProject/assets/sb-admin-v2/css/plugins/timeline/timeline.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="http://www.cs.stedwards.edu/~yuwah/SeniorProject/assets/sb-admin-v2/css/sb-admin.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Welcome, <?php echo $this->session->userdata('username'); ?>!</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="../login2/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

        </nav>
        <!-- /.navbar-static-top -->

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    
                            <li>
                                <a href="/~yuwah/SeniorProject/index.php/judge_schedule">Submit Available Times</a>
                            </li>

                </ul>
                    
        </nav>
        <!-- /.navbar-static-side -->

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <?php
                    //if(is_array($content))
                    //{
                        echo $content;
                        // foreach($content as $c)
                        // {   
                        //     $day = $c->date;
                        //     $start_time = $c->start;
                        //     $end_time = $c->end;

                        //     //date("F j, Y, g:i a");                 // March 10, 2001, 5:16 pm
                        //     //$today = date("D M j G:i:s T Y");               // Sat Mar 10 17:16:18 MST 2001

                        //     $begin_unix = $day + $start_time;
                        //     $end_unix = $day + $end_time;

                        //     $begin = date("D F j, Y, g:i a", $begin_unix);
                        //     $end = date("D F j, Y, g:i a", $end_unix);

                        //     echo $begin.' -- '.$end.'<br />';                        
                        // }                        
                    //}





                    ?>


                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="http://www.cs.stedwards.edu/~yuwah/SeniorProject/assets/sb-admin-v2/js/jquery-1.10.2.js"></script>
    <script src="http://www.cs.stedwards.edu/~yuwah/SeniorProject/assets/sb-admin-v2/js/bootstrap.min.js"></script>
    <script src="http://www.cs.stedwards.edu/~yuwah/SeniorProject/assets/sb-admin-v2/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Dashboard -->
    <script src="http://www.cs.stedwards.edu/~yuwah/SeniorProject/assets/sb-admin-v2/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="http://www.cs.stedwards.edu/~yuwah/SeniorProject/assets/sb-admin-v2/js/plugins/morris/morris.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="http://www.cs.stedwards.edu/~yuwah/SeniorProject/assets/sb-admin-v2/js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
    <script src="http://www.cs.stedwards.edu/~yuwah/SeniorProject/assets/sb-admin-v2/js/demo/dashboard-demo.js"></script>

</body>

</html>
