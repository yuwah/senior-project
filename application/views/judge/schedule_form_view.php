<html>
<head>
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url('/assets/sb-admin-v2/css/bootstrap.min.css');?>" rel="stylesheet">

    <!-- Include FontAwesome CSS if you want to use feedback icons provided by FontAwesome -->
    <link href="<?php echo base_url('/assets/sb-admin-v2/font-awesome/css/font-awesome.css');?>" rel="stylesheet">

    

    <!-- jQuery and Bootstrap JS -->
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/jquery-1.10.2.js');?>"></script>
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/bootstrap.min.js');?>"></script>

    <!-- BootstrapValidator JS -->
    <script type="text/javascript" src="<?php echo base_url('/assets/checkboxes/jquery.checkboxes-1.0.5.min.js');?>"></script>
</head>
<body>

                <?php echo $content; ?>

<script>
$(document).ready(function() {
    $(function($) {
        $('#times').checkboxes('range', true);
    });

});
</script>
</body>
</html>