<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Admin Home</title>

    <!-- Core CSS - Include with every page -->
    <link href="../assets/sb-admin-v2/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/sb-admin-v2/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="../assets/sb-admin-v2/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="../assets/sb-admin-v2/css/plugins/timeline/timeline.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="../assets/sb-admin-v2/css/sb-admin.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Debate Scheduler</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="./login2/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

        </nav>
        <!-- /.navbar-static-top -->

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    
                            <li>
                                <a href="/~yuwah/SeniorProject/index.php/create_participant">Create New Participant</a>
                            </li>
                            <li>
                                <a href="/~yuwah/SeniorProject/index.php/schedules">View Schedules</a>
                            </li>
                            <li>
                                <a href="/~yuwah/SeniorProject/index.php/schedules/remove_from_avail">Remove Student from Round</a>
                            </li>
                            <li>
                                <a href="/~yuwah/SeniorProject/index.php/create_deb_calendar">Create Debate Calendar</a>
                            </li>
                </ul>
                    
        </nav>
        <!-- /.navbar-static-side -->

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Welcome!</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="../assets/sb-admin-v2/js/jquery-1.10.2.js"></script>
    <script src="../assets/sb-admin-v2/js/bootstrap.min.js"></script>
    <script src="../assets/sb-admin-v2/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Dashboard -->
    <script src="../assets/sb-admin-v2/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="../assets/sb-admin-v2/js/plugins/morris/morris.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="../assets/sb-admin-v2/js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
    <script src="../assets/sb-admin-v2/js/demo/dashboard-demo.js"></script>

</body>

</html>
