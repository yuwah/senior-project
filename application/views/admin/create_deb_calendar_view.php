<?php echo $content; ?> 

    

    <!-- Core Scripts - Include with every page -->
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/jquery-1.10.2.js');?>"></script>
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
    
    <!-- Load jQuery and bootstrap datepicker scripts -->
            
            <script src="<?php echo base_url('/assets/sb-admin-v2/js/bootstrap-datepicker.js');?>"></script>
            <script type="text/javascript">
                // When the document is ready
                $(document).ready(function () {

                    $('.input-daterange').datepicker({
                        todayBtn: "linked"
                    });

                });
            </script>

    <!-- Page-Level Plugin Scripts - Dashboard -->
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/plugins/morris/raphael-2.1.0.min.js');?>"></script>
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/plugins/morris/morris.js');?>"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/sb-admin.js');?>"></script>

    <!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/demo/dashboard-demo.js');?>"></script>
    
    
    
    

</body>

</html>


<!--
<!DOCTYPE html>
<html>
    <head>
        <title>bootstrap datepicker examples</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap CSS and bootstrap datepicker CSS used for styling the demo pages-->
    <!-- *****
        <link rel="stylesheet" href="../assets/sb-admin-v2/css/datepicker.css">
        <link rel="stylesheet" href="../assets/sb-admin-v2/css/bootstrap.css">
    </head>
    <body>
        <div class="container">
            <div class="hero-unit">
                <div class="input-daterange" id="datepicker" >
                    <input type="text" class="input-small" name="start" />
                    <span class="add-on" style="vertical-align: top;height:20px">to</span>
                    <input type="text" class="input-small" name="end" />
                </div>
            </div>
        </div>
        <!-- Load jQuery and bootstrap datepicker scripts -->
    <!-- *****
        <script src="../assets/sb-admin-v2/js/jquery-1.9.1.min.js"></script>
        <script src="../assets/sb-admin-v2/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('.input-daterange').datepicker({
                    todayBtn: "linked"
                });
            
            });
        </script>
    </body>
</html>

-->