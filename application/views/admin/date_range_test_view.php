<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Schedules</title>

    <!-- Core CSS - Include with every page -->
    <link href="../assets/sb-admin-v2/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/sb-admin-v2/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="../assets/sb-admin-v2/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="../assets/sb-admin-v2/css/plugins/timeline/timeline.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="../assets/sb-admin-v2/css/sb-admin.css" rel="stylesheet">
    
    <!-- Bootstrap CSS and bootstrap datepicker CSS used for styling the demo pages-->
    <link rel="stylesheet" type="text/css" href="../assets/sb-admin-v2/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="../assets/sb-admin-v2/css/daterangepicker-bs3.css" />
    
</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Debate Scheduler</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="/~yuwah/SeniorProject/index.php/login"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

        </nav>
        <!-- /.navbar-static-top -->

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="#">Participants<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="/~yuwah/SeniorProject/index.php/create_participant">Create New Participant</a>
                            </li>
                            <li>
                                <a href="/~yuwah/SeniorProject/index.php/schedules">View Schedules</a>
                            </li>
                            <li>
                                <a href="/~yuwah/SeniorProject/index.php/test_page">Test Page</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dan's Range Picker Testing Page</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!--**************************************************************************************
            //Date picker things
            //************************************************************************************** -->
            
            <div class="container">
                <form>
                    <div class="form-group">
                        <label for="reservationtime">Reservation dates:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="reservation" id="reservationtime" class="col-xs-4"  />
                        </div>
                    </div>
                </form>
            </div>
            
            

            
            
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper --> 

    

    <!-- Core Scripts - Include with every page -->
    <script src="../assets/sb-admin-v2/js/jquery-1.10.2.js"></script>
    <script src="../assets/sb-admin-v2/js/bootstrap.min.js"></script>
    <script src="../assets/sb-admin-v2/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    
    <!-- Load jQuery and bootstrap datepicker scripts -->
            
            <script type="text/javascript" src="../assets/sb-admin-v2/js/moment.js"></script>
            <script type="text/javascript" src="../assets/sb-admin-v2/js/daterangepicker.js"></script>
            <script type="text/javascript">
                $(document).ready(function() {
                  $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });
                });
            </script>

    <!-- Page-Level Plugin Scripts - Dashboard -->
    <script src="../assets/sb-admin-v2/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="../assets/sb-admin-v2/js/plugins/morris/morris.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="../assets/sb-admin-v2/js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
    <script src="../assets/sb-admin-v2/js/demo/dashboard-demo.js"></script>
    
    
    
    

</body>

</html>


<!--
<!DOCTYPE html>
<html>
    <head>
        <title>bootstrap datepicker examples</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap CSS and bootstrap datepicker CSS used for styling the demo pages-->
    <!-- *****
        <link rel="stylesheet" href="../assets/sb-admin-v2/css/datepicker.css">
        <link rel="stylesheet" href="../assets/sb-admin-v2/css/bootstrap.css">
    </head>
    <body>
        <div class="container">
            <div class="hero-unit">
                <div class="input-daterange" id="datepicker" >
                    <input type="text" class="input-small" name="start" />
                    <span class="add-on" style="vertical-align: top;height:20px">to</span>
                    <input type="text" class="input-small" name="end" />
                </div>
            </div>
        </div>
        <!-- Load jQuery and bootstrap datepicker scripts -->
    <!-- *****
        <script src="../assets/sb-admin-v2/js/jquery-1.9.1.min.js"></script>
        <script src="../assets/sb-admin-v2/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('.input-daterange').datepicker({
                    todayBtn: "linked"
                });
            
            });
        </script>
    </body>
</html>

-->