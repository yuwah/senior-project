<html>
<head>
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url('/assets/sb-admin-v2/css/bootstrap.min.css');?>" rel="stylesheet">

    <!-- Include FontAwesome CSS if you want to use feedback icons provided by FontAwesome -->
    <link href="<?php echo base_url('/assets/sb-admin-v2/font-awesome/css/font-awesome.css');?>" rel="stylesheet">

    <!-- BootstrapValidator CSS -->
    <link rel="stylesheet" href="<?php echo base_url('/assets/bootstrapvalidator-dist-0/dist/css/bootstrapValidator.min.css');?>"/>

    <!-- jQuery and Bootstrap JS -->
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/jquery-1.10.2.js');?>"></script>
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/bootstrap.min.js');?>"></script>

    <!-- BootstrapValidator JS -->
    <script type="text/javascript" src="<?php echo base_url('/assets/bootstrapvalidator-dist-0/dist/js/bootstrapValidator.min.js');?>"></script>
</head>
<body>

                <?php echo $content; ?>

<script>
$(document).ready(function() {
    $('#registrationForm').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        live: 'disabled',
        fields: {
            continueRestart: {
                message: 'Please check continue',
                validators: {
                    notEmpty: {
                        message: 'Please check continue'
                    }
                }
            },
            fname: {
                trigger: 'blur',
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Please enter a first name'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'The first name can only consist of alphabetical and number'
                    },
                    different: {
                        field: 'password',
                        message: 'The username and password cannot be the same as each other'
                    }
                }
            },
            lname: {
                trigger: 'blur',
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Please enter a last name'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'The last name can only consist of alphabetical and number'
                    },
                    different: {
                        field: 'password',
                        message: 'The username and password cannot be the same as each other'
                    }
                }
            },
            username: {
                trigger: 'blur',
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Please enter a username'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'The username can only consist of alphabetical and number'
                    },
                    different: {
                        field: 'password',
                        message: 'The username and password cannot be the same as each other'
                    }
                }
            },
            email: {
                validators: {
                    //matches: '+.@stedwards.edu' {
                    //    message: 'please enter a @stedwards.edu address'
                    //},
                    notEmpty: {
                        message: 'Please enter an email address'
                    },
                    emailAddress: {
                        message: 'The email address is not a valid'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    different: {
                        field: 'username',
                        message: 'The password cannot be the same as username'
                    },
                    stringLength: {
                        min: 8,
                        message: 'The password must have at least 8 characters'
                    }
                }
            },
            parttype: {
                message: 'Please select student or judge',
                validators: {
                    notEmpty: {
                        message: 'Please select student or judge'
                    }
                }
            },
            birthday: {
                validators: {
                    notEmpty: {
                        message: 'The date of birth is required'
                    },
                    date: {
                        format: 'YYYY/MM/DD',
                        message: 'The date of birth is not valid'
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            }
        }
    });
});
</script>
</body>
</html>