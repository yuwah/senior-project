<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Passion and Civility Debate</title>

    <!-- Core CSS - Include with every page -->
    <link href="../assets/sb-admin-v2/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/sb-admin-v2/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- BootstrapValidator CSS -->
    <link rel="stylesheet" href="<?php echo base_url('/assets/bootstrapvalidator-dist-0/dist/css/bootstrapValidator.min.css');?>"/>

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="../assets/sb-admin-v2/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="../assets/sb-admin-v2/css/plugins/timeline/timeline.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="../assets/sb-admin-v2/css/sb-admin.css" rel="stylesheet">

    <!-- jQuery and Bootstrap JS -->
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/jquery-1.10.2.js');?>"></script>
    <script src="<?php echo base_url('/assets/sb-admin-v2/js/bootstrap.min.js');?>"></script>

    <!-- BootstrapValidator JS -->
    <script type="text/javascript" src="<?php echo base_url('/assets/bootstrapvalidator-dist-0/dist/js/bootstrapValidator.min.js');?>"></script>

</head>

<body>

<div id="login_form">
    
   <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $title ?></h3>
                    </div>
                    <div class="panel-body">
    
                        
                        <div class= "form-group">
                        <fieldset>
                        <?php
                        echo $content ?>
                        </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('#registrationForm').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        live: 'disabled',
        fields: {
            fname: {
                trigger: 'blur',
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Please enter a first name'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'The first name can only consist of alphabetical and number'
                    },
                    different: {
                        field: 'password',
                        message: 'The first name and password cannot be the same as each other'
                    }
                }
            },
            lname: {
                trigger: 'blur',
                message: 'The name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Please enter a last name'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'The last name can only consist of alphabetical and number'
                    },
                    different: {
                        field: 'password',
                        message: 'The last name and password cannot be the same as each other'
                    }
                }
            },
            username: {
                trigger: 'blur',
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Please enter a username'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'The username can only consist of alphabetical and number'
                    },
                    different: {
                        field: 'password',
                        message: 'The username and password cannot be the same as each other'
                    }
                }
            },
            email: {
                validators: {
                    //matches: '+.@stedwards.edu' {
                    //    message: 'please enter a @stedwards.edu address'
                    //},
                    identical: {
                        field: 'confirmEmail',
                        message: 'The email addresses are not the same'
                    },
                    notEmpty: {
                        message: 'Please enter an email address'
                    },
                    emailAddress: {
                        message: 'The email address is not a valid'
                    }
                }
            },
            confirmEmail: {
                validators: {
                    identical: {
                        field: 'email',
                        message: 'The email addresses are not the same'
                    },
                    notEmpty: {
                        message: 'Please confirm your email address'
                    },
                    emailAddress: {
                        message: 'The email address is not a valid'
                    }
                }
            },
            password: {
                validators: {
                    identical: {
                        field: 'confirmPassword',
                        message: 'The passwords are not the same'
                    },
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    different: {
                        field: 'username',
                        message: 'The password cannot be the same as username'
                    },
                    stringLength: {
                        min: 6,
                        message: 'The password must have at least 6 characters'
                    }
                }
            },
            confirmPassword: {
                validators: {
                    identical: {
                        field: 'password',
                        message: 'The passwords are not the same'
                    },
                    notEmpty: {
                        message: 'The password is required and cannot be empty'
                    },
                    different: {
                        field: 'username',
                        message: 'The password cannot be the same as username'
                    },
                    stringLength: {
                        min: 6,
                        message: 'The password must have at least 6 characters'
                    }
                }
            },
            parttype: {
                message: 'Please select student or judge',
                validators: {
                    notEmpty: {
                        message: 'Please select student or judge'
                    }
                }
            },
            major: {
                trigger: 'blur',
                message: 'The major is not valid',
                validators: {
                    notEmpty: {
                        message: 'Please enter a major'
                    },
                    different: {
                        field: 'password',
                        message: 'The major cannot be the same as each other'
                    }
                }
            },
            classification: {
                message: 'Please select your classification',
                validators: {
                    notEmpty: {
                        message: 'Please select your classification'
                    }
                }
            }
        }
    });
});
</script>
</body>
</html>