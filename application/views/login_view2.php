<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Passion and Civility Debate</title>

    <!-- Core CSS - Include with every page -->
    <link href="../assets/sb-admin-v2/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/sb-admin-v2/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="../assets/sb-admin-v2/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="../assets/sb-admin-v2/css/plugins/timeline/timeline.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="../assets/sb-admin-v2/css/sb-admin.css" rel="stylesheet">

</head>

<body>

<div id="login_form">
    
   <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Welcome to the Debate Scheduler!</h3>
                    </div>
                    <div class="panel-body">
    
                        
                        <div class= "form-group">
                        <fieldset>
                        <?php
                        echo $content ?>
                        </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>