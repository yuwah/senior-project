<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

//This class handles the header of each judge page as well as the side bar
class JudgeMenu {

	public function show_menu()
	{
		$data['judgenav'] = '<div id="wrapper">';
		$data['judgenav'] .='        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">';
		$data['judgenav'] .='            <div class="navbar-header">';
		$data['judgenav'] .='                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">';
		$data['judgenav'] .='                    <span class="sr-only">Toggle navigation</span>';
		$data['judgenav'] .='                    <span class="icon-bar"></span>';
		$data['judgenav'] .='                    <span class="icon-bar"></span>';
		$data['judgenav'] .='                    <span class="icon-bar"></span>';
		$data['judgenav'] .='                </button>';
		$data['judgenav'] .='			<a class="navbar-brand" href="/~yuwah/SeniorProject/index.php/judge">Debate Scheduler</a>';
		
		$data['judgenav'] .='            </div>';
		$data['judgenav'] .='            <!-- /.navbar-header -->';

		$data['judgenav'] .='            <ul class="nav navbar-top-links navbar-right">';
		$data['judgenav'] .='                <li class="dropdown">';
		$data['judgenav'] .='                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">';
		$data['judgenav'] .='                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>';
		$data['judgenav'] .='                    </a>';
		$data['judgenav'] .='                    <ul class="dropdown-menu dropdown-user">';
		$data['judgenav'] .='                        <li><a href="/~yuwah/SeniorProject/index.php/judge/account"><i class="fa fa-user fa-fw"></i> Account</a>';
		$data['judgenav'] .='                        </li>';
		// $data['judgenav'] .='                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>';
		// $data['judgenav'] .='                        </li>';
		$data['judgenav'] .='                        <li class="divider"></li>';
		$data['judgenav'] .='                        <li><a href="/~yuwah/SeniorProject/index.php/login2/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>';
		$data['judgenav'] .='                        </li>';
		$data['judgenav'] .='                    </ul>';
		$data['judgenav'] .='                    <!-- /.dropdown-user -->';
		$data['judgenav'] .='                </li>';
		$data['judgenav'] .='                <!-- /.dropdown -->';
		$data['judgenav'] .='            </ul>';
		$data['judgenav'] .='            <!-- /.navbar-top-links -->';

		$data['judgenav'] .='        </nav>';
		$data['judgenav'] .='        <!-- /.navbar-static-top -->';

		$data['judgenav'] .='        <nav class="navbar-default navbar-static-side" role="navigation">';
		$data['judgenav'] .='            <div class="sidebar-collapse">';
		$data['judgenav'] .='                <ul class="nav" id="side-menu">';
                    
		$data['judgenav'] .='                            <li>';
		$data['judgenav'] .='                                <a href="/~yuwah/SeniorProject/index.php/judge/judge_schedule">Submit Available Times</a>';
		$data['judgenav'] .='                            </li>';
		$data['judgenav'] .='                            <li>';
		$data['judgenav'] .='                                <a href="/~yuwah/SeniorProject/index.php/judge/submitted_times">View My Submitted Times</a>';
		$data['judgenav'] .='                            </li>';
                        
		$data['judgenav'] .='                </ul>';
                    
		$data['judgenav'] .='        </nav>';
		$data['judgenav'] .='        <!-- /.navbar-static-side -->';

		$CI =& get_instance();
		$CI->load->view('judge/judge_view2', $data);




	}

}