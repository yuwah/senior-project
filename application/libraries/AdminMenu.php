<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

//This class handles the header of each admin page as well as the side bar
class AdminMenu {

    public function show_menu()
    {
    	$data['adminnav'] = '<div id="wrapper">';
        $data['adminnav'] .= '<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">';
        $data['adminnav'] .= '<div class="navbar-header">';
        $data['adminnav'] .=  	'<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">';
        $data['adminnav'] .=			'<span class="sr-only">Toggle navigation</span>';
        $data['adminnav'] .=			'<span class="icon-bar"></span>';
		$data['adminnav'] .=			'<span class="icon-bar"></span>';
		$data['adminnav'] .=			'<span class="icon-bar"></span>';
		$data['adminnav'] .=			'</button>';
		$data['adminnav'] .=		'<a class="navbar-brand" href="/~yuwah/SeniorProject/index.php/admin">Debate Scheduler</a>';
		$data['adminnav'] .= '</div>'; //navbar-header

		$data['adminnav'] .= '			<ul class="nav navbar-top-links navbar-right">';
		$data['adminnav'] .= '                <li class="dropdown">';
		$data['adminnav'] .= '                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">';
		$data['adminnav'] .= '                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>';
		$data['adminnav'] .= '                   </a>';
		$data['adminnav'] .= '                    <ul class="dropdown-menu dropdown-user">';
		// $data['adminnav'] .= '                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>';
		// $data['adminnav'] .= '                        </li>';
		// $data['adminnav'] .= '                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>';
		// $data['adminnav'] .= '                        </li>';
		$data['adminnav'] .= '                        <li class="divider"></li>';
		$data['adminnav'] .= '                        <li><a href="/~yuwah/SeniorProject/index.php/login2/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>';
		$data['adminnav'] .= '                        </li>';
		$data['adminnav'] .= '                    </ul>';
		$data['adminnav'] .= '                    <!-- /.dropdown-user -->';
		$data['adminnav'] .= '                </li>';
		$data['adminnav'] .= '                <!-- /.dropdown -->';
		$data['adminnav'] .= '            </ul>'; //navbar-top-links
        $data['adminnav'] .= '</nav>'; //end navbar-static-top

		$data['adminnav'] .= '        <nav class="navbar-default navbar-static-side" role="navigation">';
		$data['adminnav'] .= '            <div class="sidebar-collapse">';
		$data['adminnav'] .= '                <ul class="nav" id="side-menu">';

		$data['adminnav'] .= '                            <li>';
		$data['adminnav'] .= '                                <a href="/~yuwah/SeniorProject/index.php/admin/create_deb_calendar">Create Debate Calendar</a>';
		$data['adminnav'] .= '                            </li>';
		$data['adminnav'] .= '                            <li>';
		$data['adminnav'] .= '                                <a href="/~yuwah/SeniorProject/index.php/admin/view_calendar">View Current Calendar</a>';
		$data['adminnav'] .= '                            </li>';		                    
		// $data['adminnav'] .= '                            <li>';
		// $data['adminnav'] .= '                                <a href="/~yuwah/SeniorProject/index.php/admin/add_participant">Create New Participant</a>';
		// $data['adminnav'] .= '                            </li>';
		$data['adminnav'] .= '                            <li>';
		$data['adminnav'] .= '                                <a href="/~yuwah/SeniorProject/index.php/admin/schedules">View Schedules</a>';
		$data['adminnav'] .= '                            </li>';
		$data['adminnav'] .= '                            <li>';
		$data['adminnav'] .= '                                <a href="/~yuwah/SeniorProject/index.php/admin/remove_from_avail">Remove Student from Round</a>';
		$data['adminnav'] .= '                            </li>';
		$data['adminnav'] .= '                            <li>';
		$data['adminnav'] .= '                                <a href="/~yuwah/SeniorProject/index.php/admin/restart"><font color="red">Restart</font></a>';
		$data['adminnav'] .= '                            </li>';

		$data['adminnav'] .= '                </ul>';
                    
		$data['adminnav'] .= '        </nav>'; //navbar-static-side

		// $data['adminnav'] .= '        <div id="page-wrapper">';
		// $data['adminnav'] .= '            <div class="row">';
		// $data['adminnav'] .= '                <div class="col-lg-12">';
		// $data['adminnav'] .= '                    <h1 class="page-header">Welcome!</h1>';
		// $data['adminnav'] .= '                </div>'; //col-lg-12
		// $data['adminnav'] .= '            </div>'; //row
		// $data['adminnav'] .= '        </div>'; //page-wrapper
        
  //       $data['adminnav'] .= '</div>'; //end wrapper div


        $CI =& get_instance();
        $CI->load->view('admin/admin_view2', $data);
    }
}

/* End of file Someclass.php */