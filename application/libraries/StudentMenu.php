<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

//This class handles the header of each student page as well as the side bar
class StudentMenu {

	public function show_menu()
	{
		$data['studentnav'] = '<div id="wrapper">';
		$data['studentnav'] .='        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">';
		$data['studentnav'] .='            <div class="navbar-header">';
		$data['studentnav'] .='                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">';
		$data['studentnav'] .='                    <span class="sr-only">Toggle navigation</span>';
		$data['studentnav'] .='                    <span class="icon-bar"></span>';
		$data['studentnav'] .='                    <span class="icon-bar"></span>';
		$data['studentnav'] .='                    <span class="icon-bar"></span>';
		$data['studentnav'] .='                </button>';
		$data['studentnav'] .='			<a class="navbar-brand" href="/~yuwah/SeniorProject/index.php/student">Debate Scheduler</a>';
		
		$data['studentnav'] .='            </div>';
		$data['studentnav'] .='            <!-- /.navbar-header -->';

		$data['studentnav'] .='            <ul class="nav navbar-top-links navbar-right">';
		$data['studentnav'] .='                <li class="dropdown">';
		$data['studentnav'] .='                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">';
		$data['studentnav'] .='                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>';
		$data['studentnav'] .='                    </a>';
		$data['studentnav'] .='                    <ul class="dropdown-menu dropdown-user">';
		$data['studentnav'] .='                        <li><a href="/~yuwah/SeniorProject/index.php/student/account"><i class="fa fa-user fa-fw"></i> Account</a>';
		$data['studentnav'] .='                        </li>';
		// $data['studentnav'] .='                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>';
		// $data['studentnav'] .='                        </li>';
		$data['studentnav'] .='                        <li class="divider"></li>';
		$data['studentnav'] .='                        <li><a href="/~yuwah/SeniorProject/index.php/login2/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>';
		$data['studentnav'] .='                        </li>';
		$data['studentnav'] .='                    </ul>';
		$data['studentnav'] .='                    <!-- /.dropdown-user -->';
		$data['studentnav'] .='                </li>';
		$data['studentnav'] .='                <!-- /.dropdown -->';
		$data['studentnav'] .='            </ul>';
		$data['studentnav'] .='            <!-- /.navbar-top-links -->';

		$data['studentnav'] .='        </nav>';
		$data['studentnav'] .='        <!-- /.navbar-static-top -->';

		$data['studentnav'] .='        <nav class="navbar-default navbar-static-side" role="navigation">';
		$data['studentnav'] .='            <div class="sidebar-collapse">';
		$data['studentnav'] .='                <ul class="nav" id="side-menu">';
                    
		$data['studentnav'] .='                            <li>';
		$data['studentnav'] .='                                <a href="/~yuwah/SeniorProject/index.php/student/student_schedule">Submit Available Times</a>';
		$data['studentnav'] .='                            </li>';
		$data['studentnav'] .='                            <li>';
		$data['studentnav'] .='                                <a href="/~yuwah/SeniorProject/index.php/student/submitted_times">View My Submitted Times</a>';
		$data['studentnav'] .='                            </li>';
                        
		$data['studentnav'] .='                </ul>';
                    
		$data['studentnav'] .='        </nav>';
		$data['studentnav'] .='        <!-- /.navbar-static-side -->';

		$CI =& get_instance();
		$CI->load->view('student/student_view2', $data);




	}

}