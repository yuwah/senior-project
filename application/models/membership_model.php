<?php

class Membership_model extends CI_Model{
    public function __construct()
    {
        $this->load->database();
        $this->load->helper('url');

    
    }
    
    private function db_connection()
    {
        return $this->load->database('default', TRUE);
    }


    //checks to see if a username is already taken during signup
    function check_user_exists($username)
    {
        $debate = self::db_connection();

        //$username = $this->input->post('username'));

        // $debate->join('Admin', 'username = username');
        // $debate->join('Judge', 'username = username');
        $query = $debate->get_where('Student', array('username' => $username))->result();
        $query2 = $debate->get_where('Judge', array('username' => $username))->result();
        $query3 = $debate->get_where('Admin', array('username' => $username))->result();

        $exists = false;
        //echo 'starting out, exists equals ';

        if(!empty($query))
        {
            //echo 'exists in Student table, exists = true<br />';
            $exists = true;
        }
        elseif(!empty($query2))
        {
            //echo 'exists in Judge table, exists = true<br />';
            $exists = true;
        }
        elseif(!empty($query3))
        {
            //echo 'exists in Admin table, exists = true<br />';
            $exists = true;
        }
        //echo 'function will return '.$exists;
        return $exists;

    }

    //inserts User's signup information into the database
    function insert_participant_signup()
    {
        
        //name, email will be sent to send_email()
        $fname = $this->input->post('fname');
        $email = $this->input->post('email');

        $username = $this->input->post('username');
        $pw = $this->input->post('password');

        $db_pw = md5($pw);
        
        $data = array(
            'fname' => $this->input->post('fname'),
            'lname' => $this->input->post('lname'),
            'username' => $username,
            'email' => $this->input->post('email'),
            'major' => $this->input->post('major'),
            'classID' => $this->input->post('classification'),
            'accessLevelID' => 3,
            'password' => $db_pw,
            'qualified' => 1
        );
        // echo '<pre>';
        // echo var_dump($data);
        // echo '</pre>';
        $this->db->insert('Student', $data);
        
        //send the first name, username, and password to the send_email function
        $this->send_signup_mail($fname, $email, $username, $pw);
        return;
    }

    //inserts User's signup information into the database
    function insert_judge_signup()
    {

        //prep for creatign a default password
        // $username = $this->input->post('username');
        // $random = random_string('alnum', 5);
        // $pw = $username.$random;
        
        // //name, email will be sent to send_email()
        // $fname = $this->input->post('fname');
        // $email = $this->input->post('email');

        //name, email will be sent to send_email()
        $fname = $this->input->post('fname');
        $email = $this->input->post('email');

        $username = $this->input->post('username');
        $pw = $this->input->post('password');

        $db_pw = md5($pw);            
        
        $data = array(
            'fname' => $this->input->post('fname'),
            'lname' => $this->input->post('lname'),
            'username' => $this->input->post('username'),
            'email' => $this->input->post('email'),
            'accessLevelID' => 2,
            'password' => $db_pw
        );
        $this->db->insert('Judge', $data);
        
        //send the first name, username, and password to the send_email function
        $this->send_signup_mail($fname, $email, $username, $pw);
        return;
    }    

    function send_signup_mail($fname, $email, $username, $password)
    {
        $config = Array(        
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'pcdebatescheduler@gmail.com',
            'smtp_pass' => 'pcdsshmyjs',
            'smtp_timeout' => '4',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1',
            'newline' => "\r\n"
        );
 
        $this->load->library('email', $config);
        //$this->email->set_newline("\r\n");
        
        // $message = '<html><body';
        // $message .= 'Hi, '.$fname.'! ';
        // $message .= 'Welcome to the Passion and Civility Debate Scheduler. Below is your username and temporary password.\n\n';
        // $message .= 'Username: '.$username.'\nPassword: '.$password.'\n\nClick <a href =http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/>HERE</a> to log in';

        $message = 'Hi, '.$fname.'! Welcome to the Passion and Civility Debate Scheduler. Be sure to use your username (<b>'.$username.'</b>) and password to log in and submit what times you are free to debate.<br /><br />Click <a href ="http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/login2">HERE</a> to log in.<br /><br /><i>If you are having problems, <b>do not reply to this email</b>. Contact Casey Burkhart</i>.';
        $this->email->from('pcdebatescheduler@gmail.com', 'Passion and Civility Debate');
        $this->email->to($email);

        $this->email->subject('Passion and Civility Debate');
        $this->email->message($message);    

        $this->email->send();

        return;

        //echo $this->email->print_debugger();
    }

    //go back later & hash passwords
    function validate()
    {
        $this->db->where('username', $this->input->post('username'));
        $this->db->where('password', $this->input->post('password'));
        $query = $this->db->get('Admin')->result();
        
        if(count($query) == 1)
        {
            foreach($query as $q)
            {
                $acc_level = $q->accessLevelID;
                return array('1', $acc_level);
            }
        }

        $this->db->where('username', $this->input->post('username'));
        $this->db->where('password', md5($this->input->post('password')));
        $query2 = $this->db->get('Judge')->result();
        
        if(count($query2) == 1)
        {
            foreach($query2 as $q2)
            {
                $acc_level = $q2->accessLevelID;
                return array('1', $acc_level);
            }
        }
        
        $this->db->where('username', $this->input->post('username'));
        $this->db->where('password', md5($this->input->post('password')));
        $query3 = $this->db->get('Student')->result();
        
        if(count($query3) == 1)
        {
            foreach($query3 as $q3)
            {
                $acc_level = $q3->accessLevelID;
                return array('1', $acc_level);
            }
        }
        
    }
    
    
    
    
}