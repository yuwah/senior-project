<?php
class Debate_model extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('url');

	
	}
	
	private function db_connection()
	{
		return $this->load->database('default', TRUE);
	}


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////       //////////////////////////////////////////////
    /////////////////////////////////////////////// DROPS //////////////////////////////////////////////
    ///////////////////////////////////////////////       //////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    function drop_from_savail($username)
    {
        // $this->db->where('username', $username);
        // $this->db->delete('SAvailability');

        $this->db->where('username', $username);
        $this->db->update('Student', array('qualified' => 0));

        return;
    }

    //This function truncates the Date, DateTime, SAvailability, and JAvailability tables
    //Update all students 'qualified' value to be 1.
    function restart()
    {
        $debate = self::db_connection();
        $debate->truncate('Date');
        $debate->truncate('DateTime');
        $debate->truncate('SAvailability');
        $debate->truncate('JAvailability');
        $debate->update('Student', array('qualified' =>1));

        return;
    }
	
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////       //////////////////////////////////////////////
	///////////////////////////////////////////////SELECTS//////////////////////////////////////////////
    ///////////////////////////////////////////////       //////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
	public function get_all_students()
	{
		$debate = self::db_connection();
        $debate->from('Student');
        $debate->order_by('fname asc, lname asc');
        $query = $debate->get()->result();
		// $query = $debate->get('Student')->result();
		return $query;
	
	}

    //This function checks to see if a debate calendar is empty. If it is, it returns true. If not, it returns false
    function is_cal_empty()
    {
        $debate = self::db_connection();
        $query = $debate->get('Date')->result();

        //if the Date table is empty, return true
        if(empty($query))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Function used on Student side to view what times a Student has already submitted
    //selects all the records in the SAvailability table that have the current session's username
    function student_submitted_times()
    {
        $debate = self::db_connection();

        $username = $this->session->userdata('username');
        $debate->select('*');
        $debate->from('SAvailability');
        $debate->join('DateTime', 'DateTime.dateTimeID = SAvailability.dateTimeID', 'left');
        $debate->join('Date','Date.dateID = DateTime.dateID', 'left');
        $debate->join('Time', 'Time.timeID = DateTime.timeID', 'left');
        $debate->where('username', $username);
        $query = $debate->get()->result();

        // echo '<pre>';
        // echo var_dump($query);
        // echo '</pre>';

        return $query;
    }

    //Function used on Judge side to view what times a Judge has already submitted
    //selects all the records in the SAvailability table that have the current session's username
    function judge_submitted_times()
    {
        $debate = self::db_connection();

        $username = $this->session->userdata('username');
        $debate->select('*');
        $debate->from('JAvailability');
        $debate->join('DateTime', 'DateTime.dateTimeID = JAvailability.dateTimeID', 'left');
        $debate->join('Date','Date.dateID = DateTime.dateID', 'left');
        $debate->join('Time', 'Time.timeID = DateTime.timeID', 'left');
        $debate->where('username', $username);
        $query = $debate->get()->result();

        // echo '<pre>';
        // echo var_dump($query);
        // echo '</pre>';

        return $query;
    }



    //function is sent a username and database returns an array with 
    //  just the students fname, lname, email, major, class, and encrypted password
    function get_student($username)
    {
        $debate = self::db_connection();
        $debate->join('Classification', 'Student.classID = Classification.classID');
        $query = $debate->get_where('Student', array('username' => $username))->result();

        foreach($query as $q)
        {
            $student = array(
                        'fname' => $q->fname,
                        'lname' => $q->lname,
                        'email' => $q->email,
                        'major' => $q->major,
                        'class' => $q->class_title,
                        'pw' => $q->password
                        );
        }
        return $student;

        // echo '<pre>';
        // echo var_dump($student);
        // echo '</pre>';

    }

    //function is sent a username and database returns an array with 
    //  just the judge fname, lname, email, major, class, and encrypted password
    function get_judge($username)
    {
        $debate = self::db_connection();
        $query = $debate->get_where('Judge', array('username' => $username))->result();

        foreach($query as $q)
        {
            $judge = array(
                        'fname' => $q->fname,
                        'lname' => $q->lname,
                        'email' => $q->email,
                        'pw' => $q->password
                        );
        }
        return $judge;

        // echo '<pre>';
        // echo var_dump($judge);
        // echo '</pre>';

    }
    
    public function get_all_judges()
    {
        $debate = self::db_connection();
        $debate->join('Department', 'Judge.departmentID = Department.departmentID');
        $query = $debate->get('Judge')->result();
        return $query;
    }

    //check if a student is in the SAvailability table based on the username
    function is_stu_avail($username)
    {
        $debate = self::db_connection();
        $query = $debate->get_where('Student', array('username' => $username, 'qualified' => 1))->result();

        return $query;

    }

    function is_stu_out($username)
    {
        $debate = self::db_connection();
        $query = $debate->get_where('Student', array('username' => $username, 'qualified' => 0))->result();

        return $query;

    }

    //get all the available students in SAvailability table based on the dateID
    function get_avail_students($dateTimeID)
    {
        $debate = self::db_connection();
        $debate->join('Student', 'SAvailability.username = Student.username');
        $debate->join('Classification', 'Student.classID = Classification.classID');
        $query = $debate->get_where('SAvailability', array('dateTimeID' => $dateTimeID, 'qualified' => 1))->result();

        
        // echo '<pre>';
        // echo var_dump($query);
        // echo '</pre>';

        return $query;
    }

    //get all the available students
    function get_all_avail_students()
    {
        $debate = self::db_connection();
        $debate->join('Student', 'SAvailability.username = Student.username');
        $query = $debate->get('SAvailability')->result();
        
        // echo '<pre>';
        // echo var_dump($query);
        // echo '</pre>';

        return $query;
    }

    //get all the available students in SAvailability table based on the dateID
    function get_avail_judges($dateTimeID)
    {
        $debate = self::db_connection();
        $debate->join('Judge', 'JAvailability.username = Judge.username');
        $query = $debate->get_where('JAvailability', array('dateTimeID' => $dateTimeID))->result();
        
        // echo '<pre>';
        // echo var_dump($query);
        // echo '</pre>';

        return $query;
    }    

    //get all the days in the Date table
    function get_all_deb_days()
    {
        $debate = self::db_connection();
        $query = $debate->get('Date')->result();
        return $query;
    }

    //get the dateID of a given unix date string
    function get_dateID($day)
    {
        $debate = self::db_connection();

        $query = $debate->get_where('Date', array('date' => $day))->result();
        foreach ($query as $q)
        {
            return $q->dateID;
        }

    }

    function get_date_options_test()
    {
        $debate = self::db_connection();
        $debate->select('*');
        $debate->from('DateTime');
        $debate->join('Date','Date.dateID = DateTime.dateID', 'left');
        $debate->join('Time', 'Time.timeID = DateTime.timeID', 'left');
        $query = $debate->get()->result();

        foreach ($query as $q)
        {
            //$date_arr[] = array('date' => $q->date);
            $day = $q->date;
            $start_time = $q->start;
            $end_time = $q->end;

            //date("F j, Y, g:i a");                 // March 10, 2001, 5:16 pm
            //$today = date("D M j G:i:s T Y");               // Sat Mar 10 17:16:18 MST 2001

            $begin_unix = $day + $start_time;
            $end_unix = $day + $end_time;

            $begin = date("D F j, Y, g:i a", $begin_unix);
            $end = date("D F j, Y, g:i a", $end_unix);

            $date_options[] = array(
                                'begin' => $begin,
                                'end' => $end);
            $data["begin"]=@$begin;


            //echo $data["begin"].'<br />';
            //$data["$end_time"]=@$data["$end_time"]+$end;

            
            // foreach($results as $result)
            // {
            //     //var_dump($result);
            //     //print "<br>";
            //     $username=$result->CheckedOutFor;
            //     $out=$result->CheckoutTime;
            //     $in=$result->CheckedInTime;
            //     $total=$in-$out;
            //     $data["$username"]=@$data["$username"]+$total;
            // }
            // arsort($data);
            // $return=array_slice($data,0,10);
            // return $return;


            //echo $date_arr['date'].'<br />';

        }
        return $query;
        //return $data;
        // foreach($date_arr as $d)
        // {
        //     echo $d['date'].'<br />';
        // }

        
        
    }

    function get_date_options()
    {
        $debate = self::db_connection();
        $debate->select('*');
        $debate->from('DateTime');
        $debate->join('Date','Date.dateID = DateTime.dateID', 'left');
        $debate->join('Time', 'Time.timeID = DateTime.timeID', 'left');
        $query = $debate->get()->result();

        foreach ($query as $q)
        {
            //$date_arr[] = array('date' => $q->date);
            $day = $q->date;
            $start_time = $q->start;
            $end_time = $q->end;

            //date("F j, Y, g:i a");                 // March 10, 2001, 5:16 pm
            //$today = date("D M j G:i:s T Y");               // Sat Mar 10 17:16:18 MST 2001

            $begin_unix = $day + $start_time;
            $end_unix = $day + $end_time;

            $begin = date("D F j, Y, g:i a", $begin_unix);
            $end = date("D F j, Y, g:i a", $end_unix);

            $date_options[] = array(
                                'begin' => $begin,
                                'end' => $end);
            $data["begin"]=@$begin;


            //echo $data["begin"].'<br />';
            //$data["$end_time"]=@$data["$end_time"]+$end;

            
            // foreach($results as $result)
            // {
            //     //var_dump($result);
            //     //print "<br>";
            //     $username=$result->CheckedOutFor;
            //     $out=$result->CheckoutTime;
            //     $in=$result->CheckedInTime;
            //     $total=$in-$out;
            //     $data["$username"]=@$data["$username"]+$total;
            // }
            // arsort($data);
            // $return=array_slice($data,0,10);
            // return $return;


            //echo $date_arr['date'].'<br />';

        }
        return $query;
        //return $data;
        // foreach($date_arr as $d)
        // {
        //     echo $d['date'].'<br />';
        // }

        
        
    }

    //returns the dateTimeID of a given dateID and timeID
    function get_datetimeID($dateID, $timeID)
    {
        $debate = self::db_connection();

        $query = $debate->get_where('DateTime', array('dateID' => $dateID, 'timeID' => $timeID))->result();
        foreach ($query as $q)
        {
            return $q->dateTimeID;
        }
    }

    //get the timeID 
    function get_timeID($time)
    {
        $debate = self::db_connection();

        $query = $debate->get_where('Time', array('start' => $time))->result();
        foreach ($query as $q)
        {
            return $q->timeID;
        }

    }

    //get all the times
    function get_times()
    {
        $debate = self::db_connection();
        $query = $debate->get('Time')->result();
        return $query;
    }

    function get_classifications()
    {
        $debate = self::db_connection();
        $query = $debate->get('Classification')->result();
        return $query;
    }

    function check_pw($username)
    {
        $debate = self::db_connection();
        $query = $debate->get_where('Student', array('username' => $username))->result();

        foreach($query as $q)
        {
            $pw = md5($this->input->post('curr_pw'));
            if($pw == $q->password)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    function check_judge_pw($username)
    {
        $debate = self::db_connection();
        $query = $debate->get_where('Judge', array('username' => $username))->result();

        foreach($query as $q)
        {
            $pw = md5($this->input->post('curr_pw'));
            //$pw = $this->input->post('curr_pw');
            if($pw == $q->password)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////       //////////////////////////////////////////////
    ///////////////////////////////////////////////INSERT //////////////////////////////////////////////
    ///////////////////////////////////////////////       //////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    //resetting Admin Password is currently disabled
    function temp_pw()
    {
        $username = $this->input->post('username');

        $debate = self::db_connection();
        $query = $debate->get_where('Student', array('username' => $username))->result();
        $query2 = $debate->get_where('Judge', array('username' => $username))->result();
        $query3 = $debate->get_where('Admin', array('username' => $username))->result();

        if(!empty($query))
        {
            //echo 'update pw in Student table<br />';
            foreach($query as $q)
            {
                $fname = $q->fname;
                $email = $q->email;

                $random = random_string('alnum', 5);
                $pw_temp = $username.$random;

                $db_pw = md5($pw_temp);
                $data = array(
                    'password' => $db_pw
                    );
                $this->db->update('Student', $data);
                $this->send_pw_email($fname, $email, $pw_temp);
            }
            return;
        }

        elseif(!empty($query2))
        {
            //echo 'update pw in Judge table<br />';
            foreach($query2 as $q2)
            {
                $fname = $q2->fname;
                $email = $q2->email;

                $random = random_string('alnum', 5);
                $pw_temp = $username.$random;

                $db_pw = md5($pw_temp);
                $data = array(
                    'password' => $db_pw
                    );
                $this->db->update('Judge', $data);
                $this->send_pw_email($fname, $email, $pw_temp);
            }
            return;
        }
        //resetting Admin Password is currently disabled
        elseif(!empty($query3))
        {
            //echo 'dont do anything, return<br />';
            return;
        }
        //echo 'function will return '.$exists;

    }

    function send_pw_email($fname, $email, $pw_temp)
    {
        $config = Array(        
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'pcdebatescheduler@gmail.com',
            'smtp_pass' => 'pcdsshmyjs',
            'smtp_timeout' => '4',
            'mailtype'  => 'html', 
            'charset'   => 'iso-8859-1',
            'newline' => "\r\n"
        );
 
        $this->load->library('email', $config);
        //$this->email->set_newline("\r\n");
        
        // $message = '<html><body';
        // $message .= 'Hi, '.$fname.'! ';
        // $message .= 'Welcome to the Passion and Civility Debate Scheduler. Below is your username and temporary password.\n\n';
        // $message .= 'Username: '.$username.'\nPassword: '.$password.'\n\nClick <a href =http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/>HERE</a> to log in';

        $message = $fname.',<br /><br />Below is your new password.<br /><br />Password: '.$pw_temp.'<br /><br />Be sure to change your password once you log in.<br />Click <a href ="http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/login2">HERE</a> to log in.<br /><br /><i>If you are having problems, <b>do not reply to this email</b>. Contact Casey Burkhart</i>.';
        $this->email->from('pcdebatescheduler@gmail.com', 'Passion and Civility Debate');
        $this->email->to($email);

        $this->email->subject('Passion and Civility Debate');
        $this->email->message($message);    

        $this->email->send();

        return;

        //echo $this->email->print_debugger();
    }    

    //sets a user's qualified status to 1 so his/her schedule appears when admin is doing a search
    function make_student_avail($username)
    {
        $this->db->where('username', $username);
        $this->db->update('Student', array('qualified' => 1));

        return;
    }

    function update_pw($username)
    {
        $debate = self::db_connection();

        $new_pw = $this->input->post('new_pw');
        $new_db_pw = md5($new_pw);

        $debate->where('username', $username);
        $debate->update('Student', array('password' => $new_db_pw));

        return;

    }

    function update_judge_pw($username)
    {
        $debate = self::db_connection();

        $new_pw = $this->input->post('new_pw');
        $new_db_pw = md5($new_pw);

        $debate->where('username', $username);
        $debate->update('Judge', array('password' => $new_db_pw));

        return;

    }

    function update_student($username)
    {
        $debate = self::db_connection();

        $major = $this->input->post('major');
        $classification = $this->input->post('classification');

        $this->db->where('username', $username);
        $this->db->update('Student', array('major' => $major, 'classID' => $classification));

        return;

    }

    function insert_student_schedule()
    {
        $debate = self::db_connection();
        $username = $this->session->userdata('username');

        $date_range = (is_array($_POST['date_range']['options'])) ? $_POST['date_range']['options'] : array();

        $debate->join('Student', 'SAvailability.username = Student.username');
        $query = $debate->get('SAvailability')->result();

        //if the query is empty, then the user has not previously submitted any times.
        //insert the posted data in the SAvailability table
        if(empty($query))
        {
            //echo 'no schedule';
            foreach($date_range as $d)
            {
                // echo '<pre>';
                // echo var_dump($d);
                // echo '</pre>';

                $data = array(
                        'username' => $username,
                        'dateTimeID' => $d);

                $debate->insert('SAvailability', $data);
            }
            return;    
        }
        //dump all the records in SAvailability with the current session username
        // insert data in post
        else
        {
            //echo 'schedule exists';
            $debate->where('username', $username);
            $debate->delete('SAvailability');

            foreach($date_range as $d)
            {
                // echo '<pre>';
                // echo var_dump($d);
                // echo '</pre>';

                $data = array(
                        'username' => $username,
                        'dateTimeID' => $d);

                $debate->insert('SAvailability', $data);
            }
            return;

        }


    }
    function insert_judge_schedule()
    {
        $debate = self::db_connection();
        $username = $this->session->userdata('username');

        $date_range = (is_array($_POST['date_range']['options'])) ? $_POST['date_range']['options'] : array();

        $debate->join('Judge', 'JAvailability.username = Judge.username');
        $query = $debate->get('JAvailability')->result();

        //if the query is empty, then the user has not previously submitted any times.
        //insert the posted data in the JAvailability table
        if(empty($query))
        {
            //echo 'no schedule';
            foreach($date_range as $d)
            {
                // echo '<pre>';
                // echo var_dump($d);
                // echo '</pre>';

                $data = array(
                        'username' => $username,
                        'dateTimeID' => $d);

                $debate->insert('JAvailability', $data);
            }
            return;    
        }
        //dump all the records in JAvailability with the current session username
        // insert data in post
        else
        {
            //echo 'schedule exists';
            $debate->where('username', $username);
            $debate->delete('JAvailability');

            foreach($date_range as $d)
            {
                // echo '<pre>';
                // echo var_dump($d);
                // echo '</pre>';

                $data = array(
                        'username' => $username,
                        'dateTimeID' => $d);

                $debate->insert('JAvailability', $data);
            }
            return;

        }


    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


    function insert_participant()
	{
        
		$parttype = $this->input->post('parttype');
        //if the admin is creating a Student account
        if($parttype == 1)
        {
            
            
            //name, email will be sent to send_email()
            $fname = $this->input->post('fname');
            $email = $this->input->post('email');


            //creating the username based on everything before the '@' in the email address
            $parts = explode('@', $email);
            $username = $parts[0];

            //prep for creatign a default password
            //$username = $this->input->post('username');
            $random = random_string('alnum', 5);
            $pw = $username.$random;

            $db_pw = md5($pw);
            
            $data = array(
                'fname' => $this->input->post('fname'),
                'lname' => $this->input->post('lname'),
                'username' => $username,
                'email' => $this->input->post('email'),
                'accessLevelID' => 3,
                'password' => $db_pw,
                'qualified' => 1
            );
            //echo '<pre>';
            //echo var_dump($data);
            //echo '</pre>';
            $this->db->insert('Student', $data);
            
            //send the first name, username, and password to the send_email function
            $this->send_mail($fname, $email, $username, $pw);
            return;
        }
        //the admin is creating a Judge account
        else
        {
            //prep for creatign a default password
            $username = $this->input->post('username');
            $random = random_string('alnum', 5);
            $pw = $username.$random;
            
            //name, email will be sent to send_email()
            $fname = $this->input->post('fname');
            $email = $this->input->post('email');
            
            $data = array(
                'fname' => $this->input->post('fname'),
                'lname' => $this->input->post('lname'),
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'accessLevelID' => 2
            );
            $this->db->insert('Judge', $data);
            
            //send the first name, username, and password to the send_email function
            $this->send_mail($fname, $email, $username, $pw);
            return;
        }
	}
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    function send_mail($fname, $email, $username, $password)
    {
        $config = Array(		
		    'protocol' => 'smtp',
		    'smtp_host' => 'ssl://smtp.googlemail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'pcdebatescheduler@gmail.com',
		    'smtp_pass' => 'pcdsshmyjs',
		    'smtp_timeout' => '4',
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1',
            'newline' => "\r\n"
		);
 
		$this->load->library('email', $config);
		//$this->email->set_newline("\r\n");
        
        // $message = '<html><body';
        // $message .= 'Hi, '.$fname.'! ';
        // $message .= 'Welcome to the Passion and Civility Debate Scheduler. Below is your username and temporary password.\n\n';
        // $message .= 'Username: '.$username.'\nPassword: '.$password.'\n\nClick <a href =http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/>HERE</a> to log in';

        $message = 'Hi, '.$fname.'! Welcome to the Passion and Civility Debate Scheduler. Below is your username and temporary password.<br /><br />Username: '.$username.'<br />Password: '.$password.'<br /><br />Click <a href ="http://cs.stedwards.edu/~yuwah/SeniorProject/index.php/login2">HERE</a> to log in.';
        $this->email->from('pcdebatescheduler@gmail.com', 'Yvonne');
        $this->email->to($email);

        $this->email->subject('Passion and Civility Debate');
        $this->email->message($message);	

        $this->email->send();

        return;

        //echo $this->email->print_debugger();
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    function create_deb_calendar_test($start, $end)
    {
        
        $debate = self::db_connection();
        $query = $debate->get('Date')->result();

        $dates = array();
        //$unixdates = array();
        $current = strtotime( $start );
        $last = strtotime( $end );

        while( $current <= $last ) {

            $dates[] = date('D F j, Y', $current );
            $current = strtotime('+1 day', $current );
        }

        foreach($dates as $d)
        {
            // echo '<pre>';
            // echo var_dump($d);
            // echo '</pre>';
            $date_array[] = array(
                                'date' => $d);
            //$debate->insert('Date', $d);
        }

        foreach($date_array as $dr)
        {
            $debate->insert('Date', $dr);
        }


        $this->add_to_datetime_test();

        return;

        //echo '<pre>';
        //echo var_dump($unixdates);
        //echo '</pre>';
    }

    function add_to_datetime_test()
    {
        $debate = self::db_connection();
        $query = $debate->get('Date')->result();

        foreach($query as $q)
        {

            $datetime_arr[] = array(
                            'dateID' => $q->dateID);
            
        }

        foreach($datetime_arr as $d)
        {
            ///****Change this & make it into a dateID-insert loop that repeats 25 times. 25 since you already know how many time records there are

            $the_dateID = $d['dateID'];
            $time_ID_arr = range(1,25);
            $num = 0;
            while ($num < 25)
            {
                $data = array(
                            'dateID' => $the_dateID,
                            'timeID' => $time_ID_arr[$num]
                            );
                // echo '<pre>';
                // echo var_dump($data);
                // echo '</pre>';
                //echo 'DATE ID: '.$the_dateID.' TIME ID: '.$time_ID_arr[$num].'<br />';
                $debate->insert('DateTime', $data);
                $num++;
            }
        }
    }



    //this function recieves a start and end date. It inserts every day in between the two dates (as well as the two start/end dates) into the database as a UNIX timestamp
    function create_deb_calendar($start, $end)
    {
        //To-do list: Create active column for dates?
        //Drop previous debate calendars?
        $dates = array();
        //$unixdates = array();
        $current = strtotime( $start );
        $last = strtotime( $end );

        while( $current <= $last ) {

            $dates[] = date( 'm/d/Y', $current );
            $current = strtotime( '+1 day', $current );
        }

        foreach($dates as $d)
        {
            $unixdays = strtotime($d);
            $unixdates[] = array(
                            'date' => $unixdays);
            //$this->db->insert('Date', $unixdays);
        }

        foreach($unixdates as $u)
        {
            //$thedate = $u['date'];
            $this->db->insert('Date', $u);
        }

        //$this->add_to_datetime();

        //echo '<pre>';
        //echo var_dump($unixdates);
        //echo '</pre>';
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    function add_to_datetime()
    {
        $debate = self::db_connection();
        $query = $debate->get('Date')->result();

        foreach($query as $q)
        {

            $datetime_arr[] = array(
                            'dateID' => $q->dateID);
            
        }

        foreach($datetime_arr as $d)
        {
            ////****Change this & make it into a dateID-insert loop that repeats 25 times. 25 since you already know how many time records there are

            // $time_query = $debate->get('Time')->result();
            // foreach($time_query as $t)
            // {
            //     $time_arr[] = array('timeID' => $t->timeID);
            //     //$debate->insert('DateTime', $d);
            // }

            $num = 1;
            while ($num < 26)
            {
                $debate->insert('DateTime', $d);
            }

            //for each one, set('timeID', $time[$i])
            
            //time_query[$i], $i++, if $i==17 set $i=1, else keep going

        
            
            //$data = array('dateID' => 1111, 'timeID' => 99);
            //$debate->where('dateID', 89);
            //$debate->update('DateTime', $data);
            /*
            $d_id = $d['dateID'];
            $i = 1;
            while ($i <= count($time_query)*16)
            {
                if ($i == 17)
                {
                    $i = 1;
                }
                else
                {
                    $debate->where('dateID', $d_id);
                    $debate->set('timeID', $time_query[$i]->timeID);
                    $debate->update('DateTime');
                    $i++;
                }
            }
            */
            //$test = $debate->get()->result();
            //echo '<pre>';
            //echo var_dump($test);
            //echo '</pre>';
            /*
            foreach($time_arr as $ta)
            {
                $theupdate = array(
                            'dateID' => $d,
                            'timeID' => $ta['timeID']);

                $debate->where('dateID', $d['dateID']);
                $debate->update('DateTime.timeID',$theupdate['timeID']);
            }
            */

            /*
            foreach($time_query as $tq)
            {
                $debate->where('dateID', $d['dateID']);
                $debate->update('DateTime',$theupdate);
            }
            

            foreach($time_arr as $ta)
            {
                $ta_id = $ta['timeID'];

                $debate->set('timeID', $ta_id);
                $debate->update('DateTime');
            }
            */

                    $d_id = $d['dateID'];
            // echo '<pre>';
            // echo var_dump($datetime_arr);
            // echo '</pre>';
            //echo $d_id.'<br />';
            $i=0;
            while ($i < count($time_arr))
            //for($i=0; $i < count($time_arr); $i++)
            {
                $thetime = $time_arr[$i]['timeID'];
                echo $thetime.'<br />';
                $theupdate = array('dateID' => $d_id, 'timeID' => $thetime);

                echo '<pre>';
                echo var_dump($theupdate);
                echo '</pre>';
                $debate->where('dateID', $d_id);
                //$debate->set('timeID', $thetime);
                $debate->update('DateTime', $theupdate);
                $i++;
            }
        

        }
        
        // echo '<pre>';
        //     echo var_dump($time_arr);
        //     //echo var_dump($time_query[0]->timeID);
        //     echo '</pre>';

        // $d_id = $d['dateID'];
        // // echo '<pre>';
        // // echo var_dump($datetime_arr);
        // // echo '</pre>';
        // //echo $d_id.'<br />';
        // for($i=0; $i < count($time_arr); $i++)
        // {
        //     $thetime = $time_arr[$i]['timeID'];
        //     echo $thetime.'<br />';
        //     $debate->where('dateID', 203);
        //     $debate->set('timeID', $thetime);
        //     $debate->update('DateTime');
            
        // }

        //echo '<pre>';
        //echo var_dump($time_arr);
        //echo '</pre>';


    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    


	/*
	public function get_kit()
	{
		$query = $this->db->get('KitProfile');
		return $query->result_array();
	}
	
	public function get_status()
	{
		$checkout = self :: db_connection();
		$query = $checkout->get('Status')->result_array();
		return $query;
		
		//$query = $this->db->get('Status');
		//return $query->result_array();
	}
	
	public function get_duration()
	{
		$checkout = self :: db_connection();
		$query = $checkout->get('Duration')->result_array();
		return $query;
		
		//$query = $this->db->get('Duration');
		//return $query->result_array();
	}
	
	function set_hardware_profile()
	{
		$data = array(
			'type' => $this->input->post('title'),
			'make' => $this->input->post('make'),
			'model' => $this->input->post('model'),
			'spec' => $this->input->post('spec'),
			'image' => $this->input->post('image'),
			'price' => $this->input->post('price'),
			'roomID' => $this->input->post('roomID'),
			'durationID' => $this->input->post('durationID'),
			'late_fee' => $this->input->post('late_fee')
		);
		return $this->db->insert('HardwareProfile', $data);
	}
    
    */


}