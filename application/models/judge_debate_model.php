<?php
class Judge_debate_model extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('url');

	
	}
	
	private function db_connection()
	{
		return $this->load->database('default', TRUE);
	}
	
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////       //////////////////////////////////////////////
	///////////////////////////////////////////////SELECTS//////////////////////////////////////////////
    ///////////////////////////////////////////////       //////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
	public function get_all_students()
	{
		$debate = self::db_connection();
		$query = $debate->get('Student')->result();
		return $query;
	
	}
    
    public function get_all_judges()
    {
        $debate = self::db_connection();
        $debate->join('Department', 'Judge.departmentID = Department.departmentID');
        $query = $debate->get('Judge')->result();
        return $query;
    }

    //get all the available students in SAvailability table based on the dateID
    function get_avail_students($dateTimeID)
    {
        $debate = self::db_connection();
        $debate->join('Student', 'SAvailability.username = Student.username');
        $query = $debate->get_where('SAvailability', array('dateTimeID' => $dateTimeID))->result();
        
        // echo '<pre>';
        // echo var_dump($query);
        // echo '</pre>';

        return $query;
    }

    //get all the days in the Date table
    function get_all_deb_days()
    {
        $debate = self::db_connection();
        $query = $debate->get('Date')->result();
        return $query;
    }

    //get the dateID of a given unix date string
    function get_dateID($day)
    {
        $debate = self::db_connection();

        $query = $debate->get_where('Date', array('date' => $day))->result();
        foreach ($query as $q)
        {
            return $q->dateID;
        }

    }

    //returns the dateTimeID of a given dateID and timeID
    function get_datetimeID($dateID, $timeID)
    {
        $debate = self::db_connection();

        $query = $debate->get_where('DateTime', array('dateID' => $dateID, 'timeID' => $timeID))->result();
        foreach ($query as $q)
        {
            return $q->dateTimeID;
        }
    }

    //get the timeID 
    function get_timeID()
    {
        $time = $this->input->post('time');

        if($time == 1)
        {
            $timeID = 1;
        }

       elseif($time == 2)
        {
            $timeID = 2;
        }

       elseif($time == 3)
        {
            $timeID = 3;
        }

       elseif($time == 4)
        {
            $timeID = 4;
        }

       elseif($time == 5)
        {
            $timeID = 5;
        }

       elseif($time == 6)
        {
            $timeID = 6;
        }

       elseif($time == 7)
        {
            $timeID = 7;
        }

       elseif($time == 8)
        {
            $timeID = 8;
        }

       elseif($time == 9)
        {
            $timeID = 9;
        }

       elseif($time == 10)
        {
            $timeID = 10;
        }

       elseif($time == 11)
        {
            $timeID = 11;
        }

       elseif($time == 12)
        {
            $timeID = 12;
        }

       elseif($time == 13)
        {
            $timeID = 13;
        }

       elseif($time == 14)
        {
            $timeID = 14;
        }

       elseif($time == 15)
        {
            $timeID = 15;
        }

       elseif($time == 16)
        {
            $timeID = 16;
        }

        return $timeID;

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////       //////////////////////////////////////////////
    ///////////////////////////////////////////////INSERT //////////////////////////////////////////////
    ///////////////////////////////////////////////       //////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    function insert_judge_schedule()
    {
        $debate = self::db_connection();
        $username = $this->session->userdata('username');


        $date_range = (is_array($_POST['date_range']['options'])) ? $_POST['date_range']['options'] : array();
        foreach($date_range as $d)
        {
            $data = array(
                    'username' => $username,
                    'dateTimeID' => $d);

            $debate->insert('JAvailability', $data);
        }
        return;
        //echo $date_range.'<br />';

    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    function insert_participant()
	{
        
		$parttype = $this->input->post('parttype');
        //if the admin is creating a Student account
        if($parttype == 1)
        {
            
            //prep for creatign a default password
            $username = $this->input->post('username');
            $random = random_string('alnum', 5);
            $pw = $username.$random;
            
            //name, email will be sent to send_email()
            $fname = $this->input->post('fname');
            $email = $this->input->post('email');
            
            $data = array(
                'fname' => $this->input->post('fname'),
                'lname' => $this->input->post('lname'),
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'accessLevelID' => 3,
                'password' => $pw
            );
            //echo '<pre>';
            //echo var_dump($data);
            //echo '</pre>';
            $this->db->insert('Student', $data);
            
            //send the first name, username, and password to the send_email function
            $this->send_mail($fname, $email, $username, $pw);
            return;
        }
        //the admin is creating a Judge account
        else
        {
            //prep for creatign a default password
            $username = $this->input->post('username');
            $random = random_string('alnum', 5);
            $pw = $username.$random;
            
            //name, email will be sent to send_email()
            $fname = $this->input->post('fname');
            $email = $this->input->post('email');
            
            $data = array(
                'fname' => $this->input->post('fname'),
                'lname' => $this->input->post('lname'),
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'accessLevelID' => 2
            );
            $this->db->insert('Judge', $data);
            
            //send the first name, username, and password to the send_email function
            $this->send_mail($fname, $email, $username, $pw);
            return;
        }
	}
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    function send_mail($fname, $email, $username, $password)
    {
        $config = Array(		
		    'protocol' => 'smtp',
		    'smtp_host' => 'ssl://smtp.googlemail.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'pcdebatescheduler@gmail.com',
		    'smtp_pass' => 'pcdsshmyjs',
		    'smtp_timeout' => '4',
		    'mailtype'  => 'text', 
		    'charset'   => 'iso-8859-1'
		);
 
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
        
        $message = 'Hi, '.$fname.'!<br /><br />Welcome to the Passion and Civility Debate Scheduler. Below is your username and temporary password.\n\nUsername: '.$username.'\nPassword: '.$password.'\n\nClick here to log in';
        $this->email->from('pcdebatescheduler@gmail.com', 'Yvonne');
        $this->email->to($email);

        $this->email->subject('Email Test');
        $this->email->message($message);	

        $this->email->send();

        echo $this->email->print_debugger();
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    //this function recieves a start and end date. It inserts every day in between the two dates (as well as the two start/end dates) into the database as a UNIX timestamp
    function create_deb_calendar($start, $end)
    {
        //To-do list: Create active column for dates?
        //Drop previous debate calendars?
        $dates = array();
        //$unixdates = array();
        $current = strtotime( $start );
        $last = strtotime( $end );

        while( $current <= $last ) {

            $dates[] = date( 'm/d/Y', $current );
            $current = strtotime( '+1 day', $current );
        }

        foreach($dates as $d)
        {
            $unixdays = strtotime($d);
            $unixdates[] = array(
                            'date' => $unixdays);
            //$this->db->insert('Date', $unixdays);
        }

        foreach($unixdates as $u)
        {
            //$thedate = $u['date'];
            $this->db->insert('Date', $u);
        }

        $this->add_to_datetime();

        //echo '<pre>';
        //echo var_dump($unixdates);
        //echo '</pre>';
    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    function add_to_datetime()
    {
        $debate = self::db_connection();
        $query = $debate->get('Date')->result();

        foreach($query as $q)
        {

            $datetime_arr[] = array(
                            'dateID' => $q->dateID);
            
        }

        foreach($datetime_arr as $d)
        {
            $time_query = $debate->get('Time')->result();
            foreach($time_query as $t)
            {
                $time_arr[] = array('timeID' => $t->timeID);
                $debate->insert('DateTime', $d);
            }

            //for each one, set('timeID', $time[$i])
            
            //time_query[$i], $i++, if $i==17 set $i=1, else keep going

        
            
            //$data = array('dateID' => 1111, 'timeID' => 99);
            //$debate->where('dateID', 89);
            //$debate->update('DateTime', $data);
            /*
            $d_id = $d['dateID'];
            $i = 1;
            while ($i <= count($time_query)*16)
            {
                if ($i == 17)
                {
                    $i = 1;
                }
                else
                {
                    $debate->where('dateID', $d_id);
                    $debate->set('timeID', $time_query[$i]->timeID);
                    $debate->update('DateTime');
                    $i++;
                }
            }
            */
            //$test = $debate->get()->result();
            //echo '<pre>';
            //echo var_dump($test);
            //echo '</pre>';
            /*
            foreach($time_arr as $ta)
            {
                $theupdate = array(
                            'dateID' => $d,
                            'timeID' => $ta['timeID']);

                $debate->where('dateID', $d['dateID']);
                $debate->update('DateTime.timeID',$theupdate['timeID']);
            }
            */

            /*
            foreach($time_query as $tq)
            {
                $debate->where('dateID', $d['dateID']);
                $debate->update('DateTime',$theupdate);
            }
            

            foreach($time_arr as $ta)
            {
                $ta_id = $ta['timeID'];

                $debate->set('timeID', $ta_id);
                $debate->update('DateTime');
            }
            */

                    $d_id = $d['dateID'];
            // echo '<pre>';
            // echo var_dump($datetime_arr);
            // echo '</pre>';
            //echo $d_id.'<br />';
            $i=0;
            while ($i < count($time_arr))
            //for($i=0; $i < count($time_arr); $i++)
            {
                $thetime = $time_arr[$i]['timeID'];
                echo $thetime.'<br />';
                $theupdate = array('dateID' => $d_id, 'timeID' => $thetime);

                echo '<pre>';
                echo var_dump($theupdate);
                echo '</pre>';
                $debate->where('dateID', $d_id);
                //$debate->set('timeID', $thetime);
                $debate->update('DateTime', $theupdate);
                $i++;
            }
        

        }
        
        // echo '<pre>';
        //     echo var_dump($time_arr);
        //     //echo var_dump($time_query[0]->timeID);
        //     echo '</pre>';

        // $d_id = $d['dateID'];
        // // echo '<pre>';
        // // echo var_dump($datetime_arr);
        // // echo '</pre>';
        // //echo $d_id.'<br />';
        // for($i=0; $i < count($time_arr); $i++)
        // {
        //     $thetime = $time_arr[$i]['timeID'];
        //     echo $thetime.'<br />';
        //     $debate->where('dateID', 203);
        //     $debate->set('timeID', $thetime);
        //     $debate->update('DateTime');
            
        // }

        //echo '<pre>';
        //echo var_dump($time_arr);
        //echo '</pre>';


    }
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    function get_date_options()
    {
        $debate = self::db_connection();
        $debate->select('*');
        $debate->from('DateTime');
        $debate->join('Date','Date.dateID = DateTime.dateID', 'left');
        $debate->join('Time', 'Time.timeID = DateTime.timeID', 'left');
        $query = $debate->get()->result();

        foreach ($query as $q)
        {
            //$date_arr[] = array('date' => $q->date);
            $day = $q->date;
            $start_time = $q->start;
            $end_time = $q->end;

            //date("F j, Y, g:i a");                 // March 10, 2001, 5:16 pm
            //$today = date("D M j G:i:s T Y");               // Sat Mar 10 17:16:18 MST 2001

            $begin_unix = $day + $start_time;
            $end_unix = $day + $end_time;

            $begin = date("D F j, Y, g:i a", $begin_unix);
            $end = date("D F j, Y, g:i a", $end_unix);

            $date_options[] = array(
                                'begin' => $begin,
                                'end' => $end);
            $data["begin"]=@$begin;


            //echo $data["begin"].'<br />';
            //$data["$end_time"]=@$data["$end_time"]+$end;

            
            // foreach($results as $result)
            // {
            //     //var_dump($result);
            //     //print "<br>";
            //     $username=$result->CheckedOutFor;
            //     $out=$result->CheckoutTime;
            //     $in=$result->CheckedInTime;
            //     $total=$in-$out;
            //     $data["$username"]=@$data["$username"]+$total;
            // }
            // arsort($data);
            // $return=array_slice($data,0,10);
            // return $return;


            //echo $date_arr['date'].'<br />';

        }
        return $query;
        //return $data;
        // foreach($date_arr as $d)
        // {
        //     echo $d['date'].'<br />';
        // }

        
        
    }
    


	/*
	public function get_kit()
	{
		$query = $this->db->get('KitProfile');
		return $query->result_array();
	}
	
	public function get_status()
	{
		$checkout = self :: db_connection();
		$query = $checkout->get('Status')->result_array();
		return $query;
		
		//$query = $this->db->get('Status');
		//return $query->result_array();
	}
	
	public function get_duration()
	{
		$checkout = self :: db_connection();
		$query = $checkout->get('Duration')->result_array();
		return $query;
		
		//$query = $this->db->get('Duration');
		//return $query->result_array();
	}
	
	function set_hardware_profile()
	{
		$data = array(
			'type' => $this->input->post('title'),
			'make' => $this->input->post('make'),
			'model' => $this->input->post('model'),
			'spec' => $this->input->post('spec'),
			'image' => $this->input->post('image'),
			'price' => $this->input->post('price'),
			'roomID' => $this->input->post('roomID'),
			'durationID' => $this->input->post('durationID'),
			'late_fee' => $this->input->post('late_fee')
		);
		return $this->db->insert('HardwareProfile', $data);
	}
    
    */


}